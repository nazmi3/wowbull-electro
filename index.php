<?php

function getIpAddress()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
        return trim($ips[count($ips) - 1]);
    } else {
        return $_SERVER['REMOTE_ADDR'];
    }
}
$ips = array("162.158.167.52", "162.158.167.166", "162.158.167.60");
if (in_array(getIpAddress(), $ips)) {
    /**
     * Front to the WordPress application. This file doesn't do anything, but loads
     * wp-blog-header.php which does and tells WordPress to load the theme.
     *
     * @package WordPress
     */

    /**
     * Tells WordPress to load the WordPress theme and output it.
     *
     * @var bool
     */
    define('WP_USE_THEMES', true);

    /** Loads the WordPress Environment and Template */
    require __DIR__ . '/wp-blog-header.php';
} else {
?>

    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <style>
            body {
                background: #fffc03;
            }

            @media (max-width: 578px) {
                .verification_successfull {
                    background-image: unset !important;
                }
            }

            .verification_successfull {
                background: #fffc03;
                background-image: url('https://wowbull.com/wp-content/uploads/2020/12/verify_lg-e1607489091226.png');
                background-size: cover;
                height: 92.5vh;
                background-position: center;
                background-repeat: no-repeat;
            }


            .columns .column.main {
                padding-bottom: 0 !important;
            }

            .cms-maintenance .container {
                display: block !important;
            }


            .all-div {
                /* padding: 100px 30px 0 30px; */
                display: flex;
                justify-content: center;
                align-items: center;
                position: relative;
            }

            /* #fffc03 */
            .center {
                /* display: flex; */
                justify-content: center;
                align-items: center;
            }

            .clock-time {
                background: black;
                color: white;
                padding: 20px 15px;
                border-radius: 12px;
                font-family: sans-serif;
                margin: 0 5px;
            }

            .clock {
                font-size: 30px;
                font-weight: bold;
                margin: 30px 0 50px 0;
            }

            .image-verify-1 {
                width: 90% !important;
                margin: 20px 0;
            }

            .image-verify-2 {
                width: 90% !important;
            }

            .image-verify-3 {
                margin: 30px;
                width: 30% !important;
            }

            .image-verify-4 {
                width: 100% !important;
            }

            .registration-countdown {
                margin-bottom: 20px;
            }


            @media (max-width: 1350px) {
                .clock-time {
                    padding: 15px 10px;
                    border-radius: 7px;
                    font-size: 26px;
                    margin: 0;
                }
            }

            @media (min-width: 768px) {
                #clock {
                    margin: 0;
                    position: absolute;
                    top: 50%;
                    right: 5%;
                    -ms-transform: translateY(-50%);
                    transform: translateY(-50%);
                }
            }


            @media (max-width: 768px) {
                .clock-time {
                    padding: 16px 9px;
                    font-size: 15px;
                    border-radius: 7px;

                    margin: 0;
                }

                .clock {
                    margin: 20px 0 20px 0;
                }

                #clock {
                    min-height: 500px;
                    right: 0;
                }
            }


            @media (max-width: 767px) {
                .hidden-xs {
                    display: none !important;
                }
            }

            @media (min-width: 768px) and (max-width: 991px) {
                .hidden-sm {
                    display: none !important;
                }
            }

            @media (min-width: 992px) and (max-width: 1199px) {
                .hidden-md {
                    display: none !important;
                }
            }

            @media (min-width: 1200px) {
                .hidden-lg {
                    display: none !important;
                }
            }
        </style>
    </head>
    <div class="row center verification_successfull">
        <div class="col-md-5 col-md-offset-7 text-center" id="clock">
            <div class="clock-container">
                <div><img src="https://staging.jianle.co/wp-content/uploads/2020/12/升级海报-03-e1607482858835.png" class="image-verify-1" /></div>
                <div class="clock text-center">
                    <span class="clock-time" id="days1">0</span>
                    <span class="clock-time" id="days2">0</span>
                    <span>:</span>
                    <span class="clock-time" id="hours1">0</span>
                    <span class="clock-time" id="hours2">0</span>
                    <span>:</span>
                    <span class="clock-time" id="minutes1">0</span>
                    <span class="clock-time" id="minutes2">0</span>
                    <span>:</span>
                    <span class="clock-time" id="seconds1">0</span>
                    <span class="clock-time" id="seconds2">0</span>
                </div>
                <div class="registration-countdown">
                    2020年12月9日 至 2020年12月11日
                </div>
                <div><img src="https://staging.jianle.co/wp-content/uploads/2020/12/升级海报-05-e1607483194186.png" class="image-verify-2" /></div>
            </div>
            <div class="hidden-sm hidden-md hidden-lg"><img src="https://staging.jianle.co/wp-content/uploads/2020/12/升级海报-04.png" class="image-verify-4" /></div>
            <!-- <div class="hidden-xs"> -->
            <img src="https://staging.jianle.co/wp-content/uploads/2020/11/wowbull-1.png" class="image-verify-3" />
            <!-- </div> -->
        </div>
    </div>
    <script>
        var countDownDate = new Date("Dec 11, 2020 10:30:00 GMT+0800").getTime();
        var x = setInterval(function() {
            var now = new Date().getTime();
            var distance = countDownDate - now;
            var days = minTwoDigits(Math.floor(distance / (1000 * 60 * 60 * 24)));
            var hours = minTwoDigits(Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)));
            var minutes = minTwoDigits(Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60)));
            var seconds = minTwoDigits(Math.floor((distance % (1000 * 60)) / 1000));

            document.getElementById("days1").innerHTML = days.charAt(0);
            document.getElementById("days2").innerHTML = days.charAt(1);
            document.getElementById("hours1").innerHTML = hours.charAt(0);
            document.getElementById("hours2").innerHTML = hours.charAt(1);
            document.getElementById("minutes1").innerHTML = minutes.charAt(0);
            document.getElementById("minutes2").innerHTML = minutes.charAt(1);
            document.getElementById("seconds1").innerHTML = seconds.charAt(0);
            document.getElementById("seconds2").innerHTML = seconds.charAt(1);
        }, 1000);

        function minTwoDigits(n) {
            return (n < 10 ? '0' : '') + n;
        }
        console.log("<?php echo getIpAddress(); ?>");
    </script>
<?php
}
