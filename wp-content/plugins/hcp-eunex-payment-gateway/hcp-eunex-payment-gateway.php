<?php

/**
 * Plugin Name: HCP Eunex Payment Gateway
 * Plugin URI: https://www.wowbull.com
 * Description: Custom payment gateway
 * Version: 1.0
 * Author: Nazmi
 * Author URI: https://www.wowbull.com
 * Class customCurrency
 */

add_action('woocommerce_order_status_pending', 'check_payment_status', 10, 1);
add_filter('woocommerce_payment_gateways', 'hcp_add_gateway_class');

function hcp_add_gateway_class($gateways)
{
    $gateways[] = 'WC_hcp_Gateway';
    return $gateways;
}

add_action('plugins_loaded', 'hcp_init_gateway_class');
function hcp_init_gateway_class()
{

    class WC_Hcp_Gateway extends WC_Payment_Gateway
    {
        public function __construct()
        {
            $this->id = 'hcp_eunex';
            $this->icon = '';
            $this->has_fields = true;
            $this->method_title = 'Eunex Gateway';
            $this->method_description = 'Description of Eunex payment gateway';

            $this->supports = array(
                'products'
            );

            $this->init_form_fields();

            $this->init_settings();
            $this->title = $this->get_option('title');
            $this->enabled = $this->get_option('enabled');
            $this->merchant_code =  $this->get_option('merchant_code');
            $this->merchant_key = $this->get_option('merchant_key');
            $this->topup_url = $this->get_option('topup_url');

            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
            add_action('wp_enqueue_scripts', array($this, 'payment_scripts'));
        }

        public function init_form_fields()
        {
            $this->form_fields = array(
                'enabled' => array(
                    'title'       => 'Enable/Disable',
                    'label'       => 'Enable Eunex Gateway',
                    'type'        => 'checkbox',
                    'description' => '',
                    'default'     => 'no'
                ),
                'title' => array(
                    'title'       => 'Title',
                    'type'        => 'text',
                    'description' => 'This controls the title which the user sees during checkout.',
                    'default'     => 'Eunex Payment Gateway',
                    'desc_tip'    => true,
                ),
                'topup_url' => array(
                    'title'       => 'Eunex Payment URL',
                    'type'        => 'text'
                ),
                'merchant_code' => array(
                    'title'       => 'Merchant Code',
                    'type'        => 'password'
                ),
                'merchant_key' => array(
                    'title'       => 'Merchant Key',
                    'type'        => 'password'
                )
            );
        }

        public function payment_scripts()
        {
            return;
        }

        public function validate_fields()
        {
            $billing_address = array(
                array('name' => 'billing_first_name', 'label' => '用户名'),
                array('name' => 'billing_country', 'label' => '国家'),
                array('name' => 'billing_state', 'label' => '州/省'),
                array('name' => 'billing_city', 'label' => '城市'),
                array('name' => 'billing_division', 'label' => '区/县'),
                array('name' => 'billing_postcode', 'label' => '邮编'),
                array('name' => 'billing_section', 'label' => '街道/镇'),
                array('name' => 'billing_house', 'label' => '详细地址'),
                array('name' => 'billing_phone', 'label' => '手机号码'),
            );
            $url = home_url($_SERVER['REQUEST_URI']);
            $order_id = get_string_between($url, 'order-pay/', '/?');

            $customerId = get_current_user_id();

            foreach ($billing_address as $address) {
                if (strpos($url, 'order-pay/') !== false) {
                    if (get_post_meta($order_id, $address['name'], true) != "" || get_post_meta($order_id, '_' . $address['name'], true) != "") {
                        update_post_meta($order_id, '_' . $address['name'], get_post_meta($order_id, '_' . $address['name'], true));
                    } else {
                        if (get_user_meta($customerId, $address['name'], true)  != "") {
                            update_post_meta($order_id, '_' . $address['name'], get_user_meta($customerId, $address['name'], true));
                        }
                    }
                } else {
                    if ($address['name'] != "billing_first_name" && empty($_POST[$address['name']])) {
                        wc_add_notice(sprintf(__('%s is a required field.', 'woocommerce'), '<strong>' . esc_html($address['label']) . '</strong>'), 'error');
                        return false;
                    }
                }
            }
            return true;
        }

        public function process_payment($order_id)
        {
            global $wpdb;
            date_default_timezone_set("UTC");
            $timestamp = time();
            $order = wc_get_order($order_id);
            $customerId = get_current_user_id();
            $user = get_userdata($customerId);
            $now = round(microtime(true) * 1000);
            $randomid = rand(100, 1000);
            $refNo = $now . $customerId . $randomid;

            // print_r($order);die;
            //// UPDATE BILLING ADDRESS ////
            update_post_meta($refNo, "order_id", $order_id);
            $data =  array(
                'refNo' => $refNo,
                'billing_first_name' => $user->first_name,
                'billing_country' => sanitize_text_field($_POST['billing_country'] ?: get_post_meta($order_id, '_billing_country', true)),
                'billing_state' => sanitize_text_field($_POST['billing_state'] ?: get_post_meta($order_id, '_billing_state', true)),
                'billing_city' => sanitize_text_field($_POST['billing_city'] ?: get_post_meta($order_id, '_billing_city', true)),
                'billing_division' => sanitize_text_field($_POST['billing_division'] ?: get_post_meta($order_id, '_billing_division', true)),
                'billing_section' => sanitize_text_field($_POST['billing_section'] ?: get_post_meta($order_id, '_billing_section', true)),
                'billing_postcode' => sanitize_text_field($_POST['billing_postcode'] ?: get_post_meta($order_id, '_billing_postcode', true)),
                'billing_house' => sanitize_text_field($_POST['billing_house'] ?: get_post_meta($order_id, '_billing_house', true)),
                'billing_phone' => sanitize_text_field($_POST['billing_phone'] ?: get_post_meta($order_id, '_billing_phone', true)),
            );

            foreach ($data as $key => $value) {
                update_post_meta($order_id, '_' . $key, $value);
                update_user_meta($customerId, $key, $value);
            }

            $items = $order->get_items();
            $total_regular_price = $total_sale_price = $total_discounted_price = 0;
            foreach ($items as $item) {
                $product = $item->get_product();
                $total_regular_price += $product->get_regular_price();
                $total_sale_price += $product->get_sale_price();
            }
            $total_discounted_price = $total_regular_price - $total_sale_price;

            //// GENERATE REDIRECTION TO PAYMENT GATEWAY URL ////
            $merchantKey = $this->get_option('merchant_key');
            $merchantCode = $this->get_option('merchant_code');
            $gxm_discount = $total_discounted_price / $total_regular_price;
            $topupurl = $this->get_option('topup_url');
            $currency_rates = get_option('eunex_currency_rates');
            $createdDate = "gZdGFvFqXbVxW4y";
            $callbackurl = add_query_arg('key', $order->get_order_key(), home_url("/checkout/order-received/" . $order_id . "/"));
            $cny = $total_regular_price;
            $usd = $total_regular_price * $currency_rates['cny2usd'];

            $param = array(
                'email' => $user->user_email,
                'baseAmount' => $cny,
                'amount' => $usd,
                'refNo' => $refNo,
                'discount' => $gxm_discount,
                'desc' => "订单 " . $order_id,
                'merchantName' => "WOWBULL",
                'timestamp' => $timestamp,
                'callbackUrl' => $callbackurl,
                'createdDate' => $createdDate,
                'hash' =>  hash("sha512", $merchantKey . $merchantCode . $refNo . $cny . $usd . $user->user_email . $gxm_discount . $createdDate . $timestamp),
            );

            //// SAVE TRANSACTION HISTORY ////
            $wpdb->insert(
                $wpdb->prefix . "transactions",
                array(
                    'order_id' => $order_id,
                    'ref_no' => $refNo,
                    'base_amount' => $cny,
                    'amount' => $usd,
                    'coin_symbol' => 0,
                    'status' => 1,
                    'time' => date("Y-m-d H:i:s"),
                )
            );

            $gatewayurl = add_query_arg($param, $topupurl);
            return array(
                'result' => 'success',
                'redirect' => $gatewayurl
            );
            exit;
        }
    }
}

//// OVERRIDE CHECKOUT SUCCESS PAGE ////
add_action('woocommerce_thankyou', 'validate_eunex_payment');

function validate_eunex_payment($order_id)
{
    date_default_timezone_set("UTC");
    global $woocommerce;
    global $wpdb;
    $timestamp = time();

    $merchant = get_option('woocommerce_hcp_eunex_settings', array());
    $merchantKey = $merchant['merchant_key'];
    $merchantCode = $merchant['merchant_code']; 
    $eunexurl = get_option('eunex_url');

    $customerId = get_current_user_id();
    $user = get_userdata($customerId);
    $order = wc_get_order($order_id);
    $refNo = get_post_meta($order_id, '_refNo', true);
    $refNo = @$_GET['refNo'] ?: @$refNo;
    $hashCheck = hash("sha512", $merchantKey . $merchantCode . $refNo . $timestamp);
    $checkParam = json_encode(array(
        "hash" => $hashCheck,
        "merchantCode" => $merchantCode,
        "merchantRefNum" => $refNo,
        "timestamp" => $timestamp
    ));

    $args = array(
        'method' => 'POST',
        'headers'     => ["Content-Type" => "Application/json"],
        "body" => $checkParam
    );

    $params = $eunexurl . "api/payment-gateway/open/payment/status";
    $response = wp_remote_post($params, $args);
    // print_r($response['body']);
    // die;
    try {
        $paymentStatus = json_decode($response['body'], TRUE);
    } catch (Exception $ex) {
        wc_add_notice($ex, 'error');
        wp_redirect(home_url("/checkout"));
    }

    // print_r($order);
    if (@$paymentStatus['success'] == 1) {
        $responseHashCheck = hash("sha512", $merchantKey . $merchantCode . $refNo . @$paymentStatus['data']['timestamp'] .  @$paymentStatus['data']['transactionId'] . @$paymentStatus['data']['memberEmail'] . @$paymentStatus['data']['coinSymbol'] . @$paymentStatus['data']['amount'] . @$paymentStatus['data']['transactionFees']);
        if (
            @$paymentStatus['data']['status'] == 1 &&
            @$paymentStatus['data']['memberEmail'] == $user->user_email &&
            @$paymentStatus['data']['merchantRefNum'] == $refNo &&
            @$paymentStatus['data']['hash'] == $responseHashCheck
        ) {
            if ($order->get_status() == "pending") {
                $wpdb->update(
                    $wpdb->prefix . "transactions",
                    array(
                        'coin_symbol' =>  @$paymentStatus['data']['coinSymbol'] == 'GXM' ? 1 : 2,
                        'amount' => @$paymentStatus['data']['amount'],
                        'status' => 3,
                        'time' => date("Y-m-d H:i:s"),
                    ),
                    array('ref_no' => $refNo)
                );
                $order->payment_complete();
                wc_reduce_stock_levels($order_id);
                $order->add_order_note('您好，您的订单已付款！谢谢！', true);
                $woocommerce->cart->empty_cart();
            }
        } else {
            $wpdb->update($wpdb->prefix . "transactions", array('status' => 2, 'time' => date("Y-m-d H:i:s"),), array('ref_no' => $refNo));
            wc_add_notice('付款期间发生错误！ 请尝试再次付款或在此处查看<a href="' . home_url('/my-account/transaction-record/?id=' . $order->get_id()) . '">交易记录</a>', 'error');
            wp_redirect(home_url("/checkout"));
        }
    } else {
        $items = $woocommerce->cart->get_cart();
        $wpdb->update($wpdb->prefix . "transactions", array('status' => 2, 'time' => date("Y-m-d H:i:s"),), array('ref_no' => $refNo));

        if (@$items) {
            wc_add_notice('付款期间发生错误！ 请尝试再次付款或在此处查看<a href="' . home_url('/my-account/transaction-record/?id=' . $order->get_id()) . '">交易记录</a>', 'error');
            wp_redirect(home_url("/checkout"));
        } else {
            $order_id = $order->get_id();
            $order_key = $order->get_order_key();
            if (@$order_id && @$order_key) {
                wc_add_notice('付款期间发生错误！ 请尝试再次付款或在此处查看<a href="' . home_url('/my-account/transaction-record/?id=' . $order->get_id()) . '">交易记录</a>', 'error');
                wp_redirect(home_url("/checkout/order-pay/" . $order_id . "/?pay_for_order=true&key=" . $order_key));
            } else {
                wc_add_notice('付款期间发生错误！ 请尝试再次付款或在此处查看<a href="' . home_url('/my-account/transaction-record/?id=' . $order->get_id()) . '">交易记录</a>', 'error');
                wp_redirect(home_url("/my-account-2/orders/"));
            }
        }
    }
}

//// OVERRIDE BILLING ADDRESS FORMAT ////
add_filter('woocommerce_order_formatted_billing_address', 'woo_custom_order_formatted_billing_address', 10, 2);
function woo_custom_order_formatted_billing_address($address, $wc_order)
{
    $address = array(
        'first_name'    => $wc_order->billing_first_name,
        'email'    => $wc_order->email,
        'phone'        => $wc_order->phone,
        'country'        => $wc_order->billing_country,
        'state'            => $wc_order->billing_state,
        'city'            => $wc_order->billing_city,
        'postcode'        => $wc_order->billing_postcode,
        'division'        => $wc_order->get_meta('_billing_division'),
        'section'        => $wc_order->get_meta('_billing_section'),
        'house'        => $wc_order->get_meta('_billing_house'),
    );
    return $address;
}
