<?php

/**
 * Plugin Name: HCP Custom Settings Tab
 * Plugin URI: https://www.wowbull.com
 * Description: Custom settings tab
 * Version: 1.0
 * Author: Nazmi
 * Author URI: https://www.wowbull.com
 * Class customTab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

class WC_Settings_Tab_Eunex
{

    /*
     * Bootstraps the class and hooks required actions & filters.
     *
     */
    public static function init()
    {
        add_filter('woocommerce_settings_tabs_array', __CLASS__ . '::add_settings_tab', 50);
        add_action('woocommerce_settings_tabs_settings_tab_eunex', __CLASS__ . '::settings_tab');
        add_action('woocommerce_update_options_settings_tab_eunex', __CLASS__ . '::update_settings');
    }


    /*
     * Add a new settings tab to the WooCommerce settings tabs array.
     *
     * @param array $settings_tabs Array of WooCommerce setting tabs & their labels, excluding the Subscription tab.
     * @return array $settings_tabs Array of WooCommerce setting tabs & their labels, including the Subscription tab.
     */
    public static function add_settings_tab($settings_tabs)
    {
        $settings_tabs['settings_tab_eunex'] = __('Eunex', 'woocommerce-settings-tab-eunex');
        return $settings_tabs;
    }


    /*
     * Uses the WooCommerce admin fields API to output settings via the @see woocommerce_admin_fields() function.
     *
     * @uses woocommerce_admin_fields()
     * @uses self::get_settings()
     */
    public static function settings_tab()
    {
        woocommerce_admin_fields(self::get_settings());
    }


    /*
     * Uses the WooCommerce options API to save settings via the @see woocommerce_update_options() function.
     *
     * @uses woocommerce_update_options()
     * @uses self::get_settings()
     */
    public static function update_settings()
    {
        woocommerce_update_options(self::get_settings());
    }


    /*
     * Get all the settings for this plugin for @see woocommerce_admin_fields() function.
     *
     * @return array Array of settings for @see woocommerce_admin_fields() function.
     */
    public static function get_settings()
    {

        $settings = array(
            'section_title' => array(
                'name'     => __('Eunex', 'woocommerce-settings-tab-eunex'),
                'type'     => 'title',
                'desc'     => '',
                'id'       => 'wc_settings_tab_eunex_section_title'
            ),
            'eunex_url' => array(
                'name' => __('Eunex URL', 'woocommerce-settings-tab-eunex'),
                'type' => 'text',
                'id'   => 'eunex_url'
            ),
            'gxm_discount' => array(
                'name' => __('GXM price discount (%)', 'woocommerce-settings-tab-eunex'),
                'type' => 'text',
                'desc' => __('Percentage discount when checkout using GXM', 'woocommerce-settings-tab-eunex'),
                'id'   => 'gxm_discount'
            ),
            'usd2cny_fixed' => array(
                'name'     => __('Fixed price rate', 'text-domain'),
                'type'     => 'checkbox',
                'desc'     => __('Use ¥ fixed rate pricing.', 'text-domain'),
                'id'       => 'usd2cny_fixed',
            ),
            'fixed_usd2cny_rate' => array(
                'name' => __('¥ Price Rate', 'woocommerce-settings-tab-eunex'),
                'type' => 'text',
                'desc' => __('rate of USD to ¥', 'woocommerce-settings-tab-eunex'),
                'id'   => 'fixed_usd2cny_rate'
            ),
            'section_end' => array(
                'type' => 'sectionend',
                'id' => 'wc_settings_tab_eunex_section_end'
            )
        );

        return apply_filters('wc_settings_tab_eunex_settings', $settings);
    }
}

WC_Settings_Tab_Eunex::init();
