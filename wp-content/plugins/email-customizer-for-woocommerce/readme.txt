=== Email Customizer for WooCommerce ===
Contributors: ThemeHigh
Donate link: https://themehigh.com/
Tags: woocommerce email customizer, woocommerce email designer, woocommerce email editor, woocommerce email builder, email template designer, email template builder, woocommerce email templates, order emails, woocommerce mails, woocommerce emails, woocommerce email plugin, email customiser
Requires at least: 4.9
Tested up to: 5.5
Stable tag: 2.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Email Customizer for WooCommerce helps you customize your WooCommerce Emails with a visual template builder. Style up your emails with one or two column layouts.

== Description ==
The Email Customizer For WooCommerce plugin lets you customize your transactional emails with an intuitive template builder. The plugin allows you to add different elements in the template like text, image, divider, and a lot more. It makes your template precise and easy to understand with a layout that matches your brand style.

= ☞ Default Templates =
The plugin comes with **11 WooCommerce default email templates** for various status, which you can customize from scratch to align with your brand.

= ☞ Unlimited Rows =
With the Email customizer plugin, you can add unlimited number of rows and include the appropriate elements for your email templates.

= ☞ Live Preview =
The plugin's live preview feature displays the changes made in real-time and often helps you know how the template looks like.

= ☞ Add Multiple Elements =
The Email Customizer plugin lets you add the elements namely basic, WooCommerce, and hooks in the template, to customize it efficiently.

= ☞ Dynamic Placeholders =
Includes the option to add dynamic placeholders like customer name, site name, user name, and much more. With dummy placeholders, the template designing becomes easier and keeps the messages personalized.

Available placeholders are;

*   customer_name
*   site_name
*   account_area_url
*   user_login
*   user_pass
*   reset_password_url
*   customer_note
*   customer_full_name
*   order_id
*   order_created_date

= ☞ Test Emails =
The test email feature of the plugin helps you to understand the appearance of the email template created.

= ☞ User-Friendly Builder =
This easy-to-build email template builder provides the best user experience by helping users customize & manage the WooCommerce templates effortlessly.

= ☞ Compatibility =
The Email Customizer plugin is compatible with most of the WooCommerce plugins like [Checkout Field editor for WooCommerce](https://www.themehigh.com/product/woocommerce-checkout-field-editor-pro/?utm_source=wordpress&utm_medium=referral&utm_content=tracking), [Extra Product Options for WooCommerce](https://www.themehigh.com/product/woocommerce-extra-product-options/?utm_source=wordpress&utm_medium=referral&utm_content=tracking), etc.

= ☞ Email Elements =
- **Text:** Add text content to your email template and customize the font details, border properties, margin size, and so on.

- **Image:** Add images to your email template and edit them based on your needs.

- **Divider:** Include a single line divider in the template and select an apt style for it. Edit the style, width, height, alignment, and much more of the divider.

- **Gap:** Add a gap between other elements and define its height, border details, and background color.

= ☞ WooCommerce Elements =
- **Billing Details:** Display the billing details in your email template by separately customizing the heading and details part. You can edit the text color, size, border details, etc of the billing details.

- **Shipping Details:** Display the shipping details in your email template by customizing their properties like size, color, alignment, and a lot more.

= ☞ WooCommerce Hooks =
Add the hooks provided by WooCommerce like email header, order details, order table, order meta, customer details, etc.. You can add the required hooks to your email template based on your needs.


== 💎💎 Premium Features ==
The premium version of the [Email Customizer for WooCommerce](https://www.themehigh.com/product/woocommerce-email-customizer/?utm_source=wordpress&utm_medium=referral&utm_content=tracking) plugin comes with a wide number of features and several customization options. It includes features like adding **background images, configuring background and border properties, WPML compatibility, custom hooks, and so on**.

You can check out the demo for a more detailed overview of the features. [Demo - Email Template Builder](https://flydemos.com/wecm/wp-admin/?utm_source=wordpress&utm_medium=referral&utm_content=tracking)

= ☞ More Columns =
The pro version of the Email customizer plugin lets you add multiple column layouts. Also, you can increase the number of columns based on your preference.

= ☞ More Elements =
Using the plugin's premium version, you can add elements like social share icons, buttons, gifs, and much more to the email template.

= ☞ WooCommerce Elements & Hooks =
With the Email customizer plugin, you can include the WooCommerce elements such as email header, order details, customer details, etc. Moreover, it lets you add static and dynamic content using the custom hook.

= ☞ Edit Elements =
Define the border details, padding details, background color, etc for each email template element.

= ☞ More Customization =
The plugin provides different customization options for the elements in the template. You can define the font details, upload a background image, set the image's size & position, and perform many other customizations on the template.

= ☞ WPML Compatibility =
With the WPML compatibility feature, you can design the email templates in multiple languages.

--------------------------------------------
*Make more customisations to your WooCommerce transactional mails by upgrading to the Premium version. For the complete list of features, Please visit [Email Customizer for WooCommerce](https://www.themehigh.com/product/woocommerce-email-customizer/?utm_source=wordpress&utm_medium=referral&utm_content=tracking) plugin's official page.*

Check how it works (Live Demo): 🔗 [Email Template Builder](https://flydemos.com/wecm/wp-admin/?utm_source=wordpress&utm_medium=referral&utm_content=tracking)
--------------------------------------------

== Why ThemeHigh ==
Our plugins are genuinely made after abundant research to improve the woocommerce experience of our customers. We strive every minute to provide complete support to implement your dreamstore.

🏆 2 Million+ Customers
🏆 Quickest Turn-around Support
🏆 Most Lightweight Plugins

**See a few reviews below;**
> mengelman (@mengelman) ⭐⭐⭐⭐⭐
> Easy to use plugin and their support is extremely vigilant with finding solutions to your issues.

> asperagrafica (@asperagrafica) ⭐⭐⭐⭐⭐
> Title says it all. It’s a good plugin. It does what it’s been advertised as with the occasional confusion how some things work. But you know what? A plugin with THIS friendly and fast support is always worth 5 starts, because nothing can be overcome. I do appreciate that.

> adwitprasan240 (@adwitprasan240) ⭐⭐⭐⭐⭐
> It is a great Plugin and I am easily able to change the layout.It’s all i wanted.The support team is very quick to respond and knowledgeable.It is the one of the best plugin i have used.

For more info on ThemeHigh and Email Customizer for WooCommerce plugin in specific, check out the following:

*   The [Email Customizer for WooCommerce](https://www.themehigh.com/product/woocommerce-email-customizer/?utm_source=wordpress&utm_medium=referral&utm_content=tracking) premium plugin homepage.
*   The [Knowledgebase](https://www.themehigh.com/doc/woocommerce-email-customizer/?utm_source=wordpress&utm_medium=referral&utm_content=tracking).
*   Other [WordPress Plugins](https://www.themehigh.com/plugins/?utm_source=wordpress&utm_medium=referral&utm_content=tracking) by the ThemeHigh team.
*   Follow ThemeHigh on [Facebook](https://www.facebook.com/ThemeHigh-319611541768603/?utm_source=wordpress&utm_medium=referral&utm_content=tracking), [LinkedIn](https://www.linkedin.com/company/themehigh/?utm_source=wordpress&utm_medium=referral&utm_content=tracking), [Twitter](https://twitter.com/themehigh/?utm_source=wordpress&utm_medium=referral&utm_content=tracking) & [YouTube](https://www.youtube.com/channel/UC-_uMXaC_21j1Y2_nGjTyvg/?utm_source=wordpress&utm_medium=referral&utm_content=tracking).

== Installation ==
= Minimum Requirements =
* WooCommerce 3.0 or greater
* WordPress 4.9 or greater

= Automatic installation =
1. Log in to your WordPress dashboard.
2. Navigate to the Plugins menu, and click "Add New".
3. Search and locate 'Email Customizer for WooCommerce' plugin.
4. Click 'Install Now', and WordPress will take it from there.

= Manual installation =
Manual installation method requires downloading the 'Email Customizer for WooCommerce' plugin and uploading it to your web server via your FTP application. The WordPress codex contains [instructions on how to do this here](https://wordpress.org/support/article/managing-plugins/#manual-plugin-installation).

== Frequently Asked Questions ==
=Which all email notifications can be customized using Email Customizer for WooCommerce?=
You can customize all the default WooCommerce emails using the Email Customizer plugin.

=Can I add an element directly to the template without adding columns?=
No, you have to add columns before adding elements to the email template.

=How many columns can I add to the templates using this plugin?=
You can add 1 or 2 columns using the free version. To add more than two columns in a row, you can upgrade to the premium version of the plugin.

=Can we add a custom hook to the email template?=
Custom hook option comes with the Premium version of the Plugin. To include custom hooks, please upgrade to the premium version of the plugin.

=Are default WooCoomerce Elements customizable?=
Yes, Billing and Shipping elements are customizable.

=Can we delete the entire templates created by the plugin?=
No, you can't delete the entire template. But, you are able to reset it to the initial state. Instructions to reset the template are mentioned below:
<ol>
<li>Go to Templates submenu, under the Manage Template section, find the template to reset and click on Reset button below the label.</li>
</ol>

=How to test your Email Template?=
<ol>
<li>Navigate to Dashboard → Email Customizer</li>   
<li>In the Template Editor, Click on the Test mail button. A  pop up appears to enter email id and click on the Send button.</li>
<li>A sample email in that particular template design will be sent to the entered email address.</li>
</ol>

=Can I edit the custom email template provided with Email Customizer for WooCommerce?=
<ol>
<li>Navigate to Dashboard → Email Customizer</li>   
<li>In the Manage Template section, find the template name from the list (eg: Custom Processing Order)</li>
<li>Click on the Edit button below the label, you will be navigated to the edit screen.</li>
<li>Make necessary edits in the email template, can be a from scratch customisation. </li>
<li>Save the template</li>
</ol>

== Screenshots ==
1. Customized Email Template.
2. Add unlimited row to Email Templates. 
3. Add WooCommerce Elements to Email Templates.
4. Add WooCommerce Hooks to Email Templates.
5. Add Place Holders to Email Templates.
6. Add Images to Email Templates.
7. Manage Email Templates.
8. Assign Email Templates to WooCommerce email actions. 
9. Preview Email Template.
10. Send out test emails to confirm Email Layouts.

= 2.0.0 =
* Improved plugin UI and Builder.
* Customize all the default WooCommerce emails.
* Added new placeholders.
* Fixed the shipping block heading displayed for virtual products.
* Fixed variation data format in emails.
* Option to preview template.
* Removed filter 'thwecmf_force_settings_from_plugin'

= 1.0.5 =
* Fixed the billing and shipping blocks showing dummy data in emails

= 1.0.4 =
* Improved placeholder names.
* Updated placeholders in email template.
* WooCommerce compatible version updated.

= 1.0.3 =
* WooCommerce compatible version updated.

= 1.0.2 =
* Added new filter to remove the shop manager access to the plugin settings.

= 1.0.1 =
* WooCommerce compatible version updated.

= 1.0.0 =
* First Release