<?php

/**
 * Plugin Name: HCP Custom Currency
 * Plugin URI: https://www.wowbull.com
 * Description: Custom currency display
 * Version: 1.0
 * Author: Nazmi
 * Author URI: https://www.wowbull.com
 * Class customCurrency
 */

/**
 * Show sale prices at the checkout.
 */
add_filter('woocommerce_cart_item_subtotal', 'my_custom_show_sale_price_at_checkout', 10, 3);
function my_custom_show_sale_price_at_checkout($subtotal, $cart_item, $cart_item_key)
{
    /** @var WC_Product $product */
    $product = $cart_item['data'];
    $quantity = $cart_item['quantity'];
    if (!$product) {
        return $subtotal;
    }

    $regular_price = $sale_price = $suffix = '';
    if ($product->is_taxable()) {
        if ('excl' === WC()->cart->tax_display_cart) {
            $regular_price = wc_get_price_excluding_tax($product, array('price' => $product->get_regular_price(), 'qty' => $quantity));
            $sale_price    = wc_get_price_excluding_tax($product, array('price' => $product->get_sale_price(), 'qty' => $quantity));
            if (WC()->cart->prices_include_tax && WC()->cart->tax_total > 0) {
                $suffix .= ' <small class="tax_label">' . WC()->countries->ex_tax_or_vat() . '</small>';
            }
        } else {
            $regular_price = wc_get_price_including_tax($product, array('price' => $product->get_regular_price(), 'qty' => $quantity));
            $sale_price = wc_get_price_including_tax($product, array('price' => $product->get_sale_price(), 'qty' => $quantity));
            if (!WC()->cart->prices_include_tax && WC()->cart->tax_total > 0) {
                $suffix .= ' <small class="tax_label">' . WC()->countries->inc_tax_or_vat() . '</small>';
            }
        }
    } else {
        $regular_price    = $product->get_regular_price() * $quantity;
        $sale_price       = $product->get_sale_price() * $quantity;
    }
    if ($product->is_on_sale() && $regular_price > $sale_price) {
        $price = wc_format_sale_price(
            wc_get_price_to_display($product, array('price' => $product->get_regular_price(), 'qty' => $quantity)),
            wc_get_price_to_display($product, array('qty' => $quantity))
        ) . $product->get_price_suffix();
    } else {
        $price = wc_price($regular_price) . $product->get_price_suffix();
    }
    $price = $price . $suffix;
    // return " reg : "  . $regular_price .  " sale : " . $sale_price;
    return $price;
}

add_filter('woocommerce_show_variation_price', 'filter_show_variation_price', 10, 3);
function filter_show_variation_price($condition, $product, $variation)
{
    if ($variation->get_price() === "") return false;
    else return true;
}

// function electro_get_price_html_from_to($price, $from, $to, $product)
// {
//     $style = electro_get_single_product_style();
//     if (!(is_product() && 'extended' === $style)) {
//         $price = '<del>' . ((is_numeric($from)) ? wc_price($from) : $from) . '</del> <ins>' . ((is_numeric($to)) ? wc_price($to) : $to) . '</ins>';
//     }
//     return apply_filters('electro_get_price_html_from_to', $price, $from, $to, $product);
// }

function electro_wc_format_sale_price($price, $regular_price, $sale_price)
{
    $price = '<del>' . (is_numeric($regular_price) ? wc_price($regular_price) : $regular_price) . '</del><ins>' . (is_numeric($sale_price) ? wc_price($sale_price) : $sale_price) . '</ins>';
    return apply_filters('electro_wc_format_sale_price', $price, $regular_price, $sale_price);
}

add_filter('woocommerce_variable_sale_price_html',  'lw_variable_product_price',  10,  2);
add_filter('woocommerce_variable_price_html', 'lw_variable_product_price', 10, 2);

function lw_variable_product_price($v_price, $v_product)
{
    $prod_prices = array(
        $v_product->get_variation_price('min', true),
        $v_product->get_variation_price('max', true)
    );
    $prod_price = $prod_prices[0] !== $prod_prices[1] ? sprintf(
        __('%1$s', 'woocommerce'),
        wc_price($prod_prices[0])
    ) : wc_price($prod_prices[0]);

    $regular_prices = array(
        $v_product->get_variation_regular_price('min', true),
        $v_product->get_variation_regular_price('max', true)
    );
    sort($regular_prices);
    $regular_price = $regular_prices[0] !== $regular_prices[1] ? sprintf(
        __('%1$s', 'woocommerce'),
        wc_price($regular_prices[0])
    ) : wc_price($regular_prices[0]);

    if ($prod_price !== $regular_price) {
        $prod_price = '<del>' . $regular_price . $v_product->get_price_suffix() . '</del> <ins>' .
            $prod_price . $v_product->get_price_suffix() . '</ins>';
    }
    return $prod_price;
}

function hcp_change_product_price_html($price_html, $product)
{
    $wc_currency = get_option('woocommerce_currency');
    $currency_symbol = get_default_currency_symbol($wc_currency);
    $sale_price = $product->sale_price ?  $product->sale_price : $product->regular_price;
    $price_html .= add_custom_price($product->regular_price, $sale_price, $currency_symbol);
    // $v_product_types = array('variable');
    // if (in_array($product->product_type, $v_product_types) && !(is_shop())) {
    //     return '';
    // }
    return $price_html;
}
add_filter('woocommerce_get_price_html', 'hcp_change_product_price_html', 11, 2);

function hcp_change_product_price_cart($price, $cart_item, $cart_item_key)
{
    // print_r($cart_item);
    $wc_currency = get_option('woocommerce_currency');
    $currency_symbol = get_default_currency_symbol($wc_currency);
    $price .= add_custom_price($cart_item['line_subtotal'], $cart_item['line_subtotal'], $currency_symbol);
    return $price;
}
add_filter('woocommerce_cart_item_price', 'hcp_change_product_price_cart', 11, 3);

/**
 * Display custom currencies
 * @return mixed|void
 */
function add_custom_price($price, $sale_price, $currency_symbol)
{
    $return = "<br><div class='custom_currency_box row'><div class='custom_currency_inner_box'>";
    $i = 1;
    $selected_currencies = get_list_currencies();
    foreach ($selected_currencies as $currencies) {
        if ($currencies['name'] == "CNY") {
            continue;
        }
        $price = str_replace(",", "", $price);
        if ($currencies['name'] == "GXM") {
            // $discounted_price = $price * (1 - (get_option('gxm_discount') / 100)); 
            $base_price = number_format(($sale_price), 2, '.', ',');
            $converted_price = number_format(($sale_price  * $currencies['rate']), 2, '.', ',');
        }
        if ($currencies['name'] == "USDT") {
            $base_price = number_format(($price), 2, '.', ',');
            $converted_price = number_format(($price  * $currencies['rate']), 2, '.', ',');
        }
        $return .= //($i > 1 ? '<div class="or-symbol">或</div>' : "") .
            "<div class='custom_currency " . $currencies['name'] . "'><div class='custom_currency_logo'>
            <img style='width:15px;height:15px' class='logo " . $currencies['name'] . "' src='" . $currencies['flag'] . "'></div>" .
            "<div class='custom_currency_text " . $currencies['name'] . "'>"
            // . $currencies['name']  . " " 
            . $converted_price
            . " (" . $currency_symbol . " " . $base_price . ")"
            . "</div></div>";
        $i++;
    }
    $return .= "</div></div>";
    return $i == 1 ? "" : $return;
}

/**
 * Get list currencies
 * @return mixed|void
 */
function get_list_currencies()
{
    $currencies = get_all_currencies();
    if (empty($currencies) or !is_array($currencies) or count($currencies) < 2) {
        $currencies = prepare_default_currencies();
    }
    return $currencies;
}

/**
 * Get currencies
 * @return mixed|void
 */
function get_all_currencies()
{
    $currencies = get_option('woocs', array());
    return $currencies;
}

function prepare_default_currencies()
{

    $default = array(
        'USD' => array(
            'name' => 'USD',
            'rate' => 1,
            'symbol' => '&#36;',
            'position' => 'right',
            'is_etalon' => 0,
            'description' => 'USA dollar',
            'hide_cents' => 0,
            'hide_on_front' => 0,
            'flag' => '',
        ),
    );
    $wc_currency = get_option('woocommerce_currency');

    switch ($wc_currency) {
        case 'USD':
            $default['EUR'] = array(
                'name' => 'EUR',
                'rate' => 0.89,
                'symbol' => '&euro;',
                'position' => 'left_space',
                'is_etalon' => 0,
                'description' => 'European Euro',
                'hide_cents' => 0,
                'hide_on_front' => 0,
                'flag' => '',
            );
            $default['USD']['is_etalon'] = 1;
            break;
        case 'EUR':
            $default['EUR'] = array(
                'name' => 'EUR',
                'rate' => 1,
                'symbol' => '&euro;',
                'position' => 'left_space',
                'is_etalon' => 1,
                'description' => 'European Euro',
                'hide_cents' => 0,
                'hide_on_front' => 0,
                'flag' => '',
            );
            $default['USD']['rate'] = 1.15;
            break;
        default:
            $default[$wc_currency] = array(
                'name' => $wc_currency,
                'rate' => 1,
                'symbol' => get_default_currency_symbol($wc_currency),
                'position' => 'left_space',
                'is_etalon' => 1,
                'description' => '',
                'hide_cents' => 0,
                'hide_on_front' => 0,
                'flag' => '',
            );
            $default['USD']['rate'] = 1.15;
            $default['USD']['description'] = esc_html__('change the rate and this description to the right values', 'woocommerce-currency-switcher');
            break;
    }

    return $default;
}

//just need it to set default data after the plugin installing
function get_default_currency_symbol($currency)
{
    $symbols = get_symbols_set();
    return isset($symbols[$currency]) ? $symbols[$currency] : '&#36;';
}

function get_symbols_set()
{
    return array(
        'USD' => '&#36;',
        'EUR' => '&euro;',
        'GBP' => '&pound;',
        'UAH' => '&#1075;&#1088;&#1085;.',
        'RUB' => '&#1088;&#1091;&#1073;.',
        'AED' => '&#x62f;.&#x625;',
        'AFN' => '&#x60b;',
        'ALL' => 'L',
        'AMD' => 'AMD',
        'ANG' => '&fnof;',
        'AOA' => 'Kz',
        'ARS' => '&#36;',
        'AUD' => '&#36;',
        'AWG' => 'Afl.',
        'AZN' => 'AZN',
        'BAM' => 'KM',
        'BBD' => '&#36;',
        'BDT' => '&#2547;&nbsp;',
        'BGN' => '&#1083;&#1074;.',
        'BHD' => '.&#x62f;.&#x628;',
        'BIF' => 'Fr',
        'BMD' => '&#36;',
        'BND' => '&#36;',
        'BOB' => 'Bs.',
        'BRL' => '&#82;&#36;',
        'BSD' => '&#36;',
        'BTC' => '&#3647;',
        'BTN' => 'Nu.',
        'BWP' => 'P',
        'BYR' => 'Br',
        'BYN' => 'Br',
        'BZD' => '&#36;',
        'CAD' => '&#36;',
        'CDF' => 'Fr',
        'CHF' => '&#67;&#72;&#70;',
        'CLP' => '&#36;',
        'CNY' => '&yen;',
        'COP' => '&#36;',
        'CRC' => '&#x20a1;',
        'CUC' => '&#36;',
        'CUP' => '&#36;',
        'CVE' => '&#36;',
        'CZK' => '&#75;&#269;',
        'DJF' => 'Fr',
        'DKK' => 'DKK',
        'DOP' => 'RD&#36;',
        'DZD' => '&#x62f;.&#x62c;',
        'EGP' => 'EGP',
        'ERN' => 'Nfk',
        'ETB' => 'Br',
        'FJD' => '&#36;',
        'FKP' => '&pound;',
        'GEL' => '&#x10da;',
        'GGP' => '&pound;',
        'GHS' => '&#x20b5;',
        'GIP' => '&pound;',
        'GMD' => 'D',
        'GNF' => 'Fr',
        'GTQ' => 'Q',
        'GYD' => '&#36;',
        'HKD' => '&#36;',
        'HNL' => 'L',
        'HRK' => 'Kn',
        'HTG' => 'G',
        'HUF' => '&#70;&#116;',
        'IDR' => 'Rp',
        'ILS' => '&#8362;',
        'IMP' => '&pound;',
        'INR' => '&#8377;',
        'IQD' => '&#x639;.&#x62f;',
        'IRR' => '&#xfdfc;',
        'IRT' => '&#x062A;&#x0648;&#x0645;&#x0627;&#x0646;',
        'ISK' => 'kr.',
        'JEP' => '&pound;',
        'JMD' => '&#36;',
        'JOD' => '&#x62f;.&#x627;',
        'JPY' => '&yen;',
        'KES' => 'KSh',
        'KGS' => '&#x441;&#x43e;&#x43c;',
        'KHR' => '&#x17db;',
        'KMF' => 'Fr',
        'KPW' => '&#x20a9;',
        'KRW' => '&#8361;',
        'KWD' => '&#x62f;.&#x643;',
        'KYD' => '&#36;',
        'KZT' => 'KZT',
        'LAK' => '&#8365;',
        'LBP' => '&#x644;.&#x644;',
        'LKR' => '&#xdbb;&#xdd4;',
        'LRD' => '&#36;',
        'LSL' => 'L',
        'LYD' => '&#x644;.&#x62f;',
        'MAD' => '&#x62f;.&#x645;.',
        'MDL' => 'MDL',
        'MGA' => 'Ar',
        'MKD' => '&#x434;&#x435;&#x43d;',
        'MMK' => 'Ks',
        'MNT' => '&#x20ae;',
        'MOP' => 'P',
        'MRO' => 'UM',
        'MUR' => '&#x20a8;',
        'MVR' => '.&#x783;',
        'MWK' => 'MK',
        'MXN' => '&#36;',
        'MYR' => '&#82;&#77;',
        'MZN' => 'MT',
        'NAD' => '&#36;',
        'NGN' => '&#8358;',
        'NIO' => 'C&#36;',
        'NOK' => '&#107;&#114;',
        'NPR' => '&#8360;',
        'NZD' => '&#36;',
        'OMR' => '&#x631;.&#x639;.',
        'PAB' => 'B/.',
        'PEN' => 'S/.',
        'PGK' => 'K',
        'PHP' => '&#8369;',
        'PKR' => '&#8360;',
        'PLN' => '&#122;&#322;',
        'PRB' => '&#x440;.',
        'PYG' => '&#8370;',
        'QAR' => '&#x631;.&#x642;',
        'RMB' => '&yen;',
        'RON' => 'lei',
        'RSD' => '&#x434;&#x438;&#x43d;.',
        'RWF' => 'Fr',
        'SAR' => '&#x631;.&#x633;',
        'SBD' => '&#36;',
        'SCR' => '&#x20a8;',
        'SDG' => '&#x62c;.&#x633;.',
        'SEK' => '&#107;&#114;',
        'SGD' => '&#36;',
        'SHP' => '&pound;',
        'SLL' => 'Le',
        'SOS' => 'Sh',
        'SRD' => '&#36;',
        'SSP' => '&pound;',
        'STD' => 'Db',
        'SYP' => '&#x644;.&#x633;',
        'SZL' => 'L',
        'THB' => '&#3647;',
        'TJS' => '&#x405;&#x41c;',
        'TMT' => 'm',
        'TND' => '&#x62f;.&#x62a;',
        'TOP' => 'T&#36;',
        'TRY' => '&#8378;',
        'TTD' => '&#36;',
        'TWD' => '&#78;&#84;&#36;',
        'TZS' => 'Sh',
        'UGX' => 'UGX',
        'UYU' => '&#36;',
        'UZS' => 'UZS',
        'VEF' => 'Bs F',
        'VND' => '&#8363;',
        'VUV' => 'Vt',
        'WST' => 'T',
        'XAF' => 'CFA',
        'XCD' => '&#36;',
        'XOF' => 'CFA',
        'XPF' => 'Fr',
        'YER' => '&#xfdfc;',
        'ZAR' => '&#82;',
        'ZMW' => 'ZK'
    );
}
