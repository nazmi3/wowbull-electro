<?php

/**
 * Plugin Name: HCP Custom Account Page
 * Plugin URI: https://www.wowbull.com
 * Description: Custom Account Page
 * Version: 1.0
 * Author: Nazmi
 * Author URI: https://www.wowbull.com
 * Class customCurrency
 */

function hcp_install()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "transactions";
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $table_name (
            id mediumint(9) NOT NULL AUTO_INCREMENT,
            order_id bigint(20) NOT NULL, 
            ref_no varchar(255) DEFAULT '' NOT NULL,  
            base_amount decimal(13,8) NULL,  
            amount decimal(13,8) NULL,  
            coin_symbol int(5) NULL,  
            status int(5) DEFAULT 0 NOT NULL,  
            time datetime,
            PRIMARY KEY  (id)
            ) $charset_collate;";
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);
}
register_activation_hook(__FILE__, 'hcp_install');

function hcp_add_transaction_record_endpoint()
{
    add_rewrite_endpoint('transaction-record', EP_ROOT | EP_PAGES);
}
add_action('init', 'hcp_add_transaction_record_endpoint');


// ------------------
// 2. Add new query var

function hcp_transaction_record_query_vars($vars)
{
    $vars[] = 'transaction-record';
    return $vars;
}
add_filter('query_vars', 'hcp_transaction_record_query_vars', 0);


function add_my_account_order_actions($actions, $order)
{
    $actions['transaction-record'] = array(
        'url'  => '/my-account/transaction-record/?id=' . $order->get_id(),
        'name' => '交易记录',
    );
    return $actions;
}
add_filter('woocommerce_my_account_my_orders_actions', 'add_my_account_order_actions', 10, 2);


// ------------------
// 4. Add content to the new endpoint

function hcp_transaction_record_content()
{
    global $wpdb;
    $order_id = $_GET['id'];
    $order = wc_get_order($order_id);
    $wc_currency = get_option('woocommerce_currency');
    $currency_symbol = get_default_currency_symbol($wc_currency);
    $trxs = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . $wpdb->base_prefix . "transactions WHERE order_id = $order_id ORDER BY id DESC"));
?>
    <header class="entry-header">
        <h1 class="entry-title"><?php esc_html_e('交易记录', 'woocommerce'); ?></h1>
    </header>

    <table class="shop_table">
        <thead>
            <tr>
                <th class="product-name"><?php esc_html_e('Date', 'woocommerce'); ?></th>
                <th class="product-quantity"><?php esc_html_e('商城编号', 'woocommerce'); ?></th>
                <th class="product-total"><?php esc_html_e('Totals', 'woocommerce'); ?></th>
                <th class="product-total"><?php esc_html_e('Status', 'woocommerce'); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php if (count($trxs) > 0) : ?>
                <?php foreach ($trxs as $trx) :
                    $url = add_query_arg(
                        array(
                            'key' => $order->get_order_key(),
                            'refNo' => $trx->ref_no
                        ),
                        home_url("/checkout/order-received/" . $order_id . "/")
                    );
                    switch ($trx->status) {
                        case 2:
                            $status = '<a href="' . $url . '" class="woocommerce-button button pay">' . __("Failed", 'woocommerce') . '</a>';
                            break;
                        case 3:
                            $status = __("Completed", 'woocommerce');
                            break;
                        default:
                            $status = '<a href="' . $url . '" class="woocommerce-button button pay">' . __("Pending", 'woocommerce') . '</a>';
                            break;
                    }
                ?>
                    <tr class="">
                        <td class=""><?php echo $trx->time; ?></td>
                        <td class=""><?php echo $trx->ref_no; ?></td>
                        <td class=""><?php echo $trx->status == 3 ? $trx->coin_symbol . " " . $trx->amount : $currency_symbol . " " . $trx->base_amount; ?></td>
                        <td class=""><?php echo $status; ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
<?php }

add_action('woocommerce_account_transaction-record_endpoint', 'hcp_transaction_record_content');

add_action('woocommerce_pay_order_after_submit', function () {
    $url = home_url($_SERVER['REQUEST_URI']);
    $order_id = get_string_between($url, 'order-pay/', '/?');
    if (strpos($url, 'order-pay/') !== false) {
        $order = wc_get_order($order_id);
        echo '<a class="button alt" href="' . home_url('/my-account/transaction-record/?id=' . $order->get_id()) . '">' . __("交易记录", 'woocommerce') . '</a>';
    }
});

function get_string_between($string, $start, $end)
{
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}
