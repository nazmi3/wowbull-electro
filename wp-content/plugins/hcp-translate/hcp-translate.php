<?php

/**
 * Plugin Name: HCP Translation
 * Plugin URI: https://www.wowbull.com
 * Description: Translation
 * Version: 1.0
 * Author: Nazmi
 * Author URI: https://www.wowbull.com
 */

add_filter('electro_product_categories_widget_show_all_categories_text', function () {
    return "首页";
    //// Show all categories
});

add_filter('electro_handheld_sidebar_title', function () {
    return "筛选";
    //// Filters
});

add_filter('electro_product_categories_widget_shop_all_categories_text', function () {
    return "浏览类别";
    //// Browse Categories
});

add_filter('electro_wc_ppp_text', function () {
    return "显示 %s";
    //// Show %s 
});

add_filter('electro_live_search_empty_msg', function () {
    return "无法搜寻到您输入的商品";
    //// Unable to find any products that match the current query 
});

add_filter('electro_deal_countdown_timer_info_text', function () {
    return "限时抢购！倒数："; 
});

add_filter('electro_deal_countdown_timer_clock_text', function () {
    return array(
    'days_text'     => '天',
    'hours_text'    => '小时',
    'mins_text'     => '分钟',
    'secs_text'     => '秒');
}); 

add_filter('woocommerce_product_tabs', 'woo_rename_tabs', 98);
function woo_rename_tabs($tabs)
{
    $tabs['specification']['title'] = '商品详情';
    $tabs['reviews']['title'] = '累计评价';
    return $tabs;
}

if ( ! function_exists( 'electro_template_loop_availability' ) ) { 
	function electro_template_loop_availability() {

		$availability = apply_filters( 'electro_get_availability', electro_get_availability() );

		if ( ! empty( $availability[ 'availability'] ) ) : ?>

			<div class="availability">
				<?php echo '数量: ';?> <span class="electro-stock-availability"><p class="stock <?php echo esc_attr( $availability['class'] ); ?>"><?php echo esc_html( $availability['availability'] ); ?></p></span>
			</div>

		<?php endif;
	}
}

if ( ! function_exists( 'electro_wrap_order_review' ) ) {
	function electro_wrap_order_review() {
		?><div class="order-review-wrapper">
			<h3 id="order_review_heading_v2"><?php echo '您的订单'; ?></h3><?php
	}
}

if ( ! function_exists( 'electro_deal_progress_bar' ) ) { 
	function electro_deal_progress_bar() {
		$total_stock_quantity = get_post_meta( get_the_ID(), '_total_stock_quantity', true );
		if( ! empty( $total_stock_quantity ) ) {
			$stock_quantity		= round( $total_stock_quantity );
			$stock_available 	= ( $stock = get_post_meta( get_the_ID(), '_stock', true ) ) ? round( $stock ) : 0;
			$stock_sold 	 	= ( $stock_quantity > $stock_available ? $stock_quantity - $stock_available : 0 );
			$percentage 		= ( $stock_sold > 0 ? round( $stock_sold/$stock_quantity * 100 ) : 0 );
		} else {
			$stock_available 	= ( $stock = get_post_meta( get_the_ID(), '_stock', true ) ) ? round( $stock ) : 0;
			$stock_sold 	 	= ( $total_sales = get_post_meta( get_the_ID(), 'total_sales', true ) ) ? round( $total_sales ) : 0;
			$stock_quantity		= $stock_sold + $stock_available;
			$percentage 		= ( ( $stock_available > 0 && $stock_quantity > 0 ) ? round( $stock_sold/$stock_quantity * 100 ) : 0 );
		}

		if( $stock_available > 0 ) :
		?>
		<div class="deal-progress">
			<div class="deal-stock">
				<span class="stock-sold"><?php echo '已售出:';?> <strong><?php echo esc_html( $stock_sold ); ?></strong></span>
				<span class="stock-available"><?php echo '存货:';?> <strong><?php echo esc_html( $stock_available ); ?></strong></span>
			</div>
			<div class="progress">
				<span class="progress-bar" style="<?php echo esc_attr( 'width:' . $percentage . '%' ); ?>"><?php echo esc_html( $percentage ); ?></span>
			</div>
		</div>
		<?php
		endif;
	}
}
 
function electro_onsale_product($args = array())
{

    if (is_woocommerce_activated()) {

        $defaults     = apply_filters('electro_onsale_product_default_args', array(
            'section_title'    => wp_kses_post(__('<span class="highlight">Special</span> Offer', 'electro')),
            'section_class'    => '',
            'show_savings'    => true,
            'savings_in'    => 'amount',
            'savings_text'    => '便宜',
        ));

        if (isset($args['product_choice'])) {
            switch ($args['product_choice']) {
                case 'random':
                    $args['orderby'] = 'rand';
                    break;
                case 'recent':
                    $args['orderby']     = 'date';
                    $args['order']         = 'DESC';
                    break;
                case 'specific':
                    $args['orderby']     = 'post__in';
                    $args['ids']         = $args['product_id'];
                    $args['post__in']     = array_map('trim', explode(',', $args['product_id']));
                    break;
            }
        }

        $args         = wp_parse_args(array('per_page'    => 1), $args);
        $args         = apply_filters('electro_onsale_product_args', wp_parse_args($args, $defaults));

        if (isset($args['post__in'])) {
            $products     = Electro_Products::products($args);
        } else {
            $products     = Electro_Products::sale_products($args);
        }

        extract($args);

        if ($products->have_posts()) {

            while ($products->have_posts()) : $products->the_post();

                global $product;
?>
                <section class="section-onsale-product <?php echo esc_attr($section_class); ?>">

                    <?php if (!empty($section_title) || $show_savings) : ?>

                        <header>

                            <?php if (!empty($section_title)) : ?>

                                <h2 class="h1"><?php echo wp_kses_post($section_title); ?></h2>

                            <?php endif; ?>

                            <?php if ($product->is_on_sale() && $show_savings) : ?>

                                <div class="savings">
                                    <span class="savings-text">
                                        <?php echo sprintf('%s %s', '便宜', Electro_WC_Helper::get_savings_on_sale($product, $savings_in));
                                        ?>
                                    </span>
                                </div>

                            <?php endif; ?>

                        </header>

                    <?php endif; ?>
                    <div class="onsale-products">
                        <?php wc_get_template_part('templates/contents/content', 'onsale-product'); ?>
                    </div>

                </section>

        <?php

            endwhile;

            woocommerce_reset_loop();
            wp_reset_postdata();
        }
    }
}

function electro_deal_save_label()
{
    global $product;

    $args = apply_filters('electro_deal_save_label_args', array(
        'savings_text'        => '便宜',
        'savings_in'        => 'amount'
    ));

    if (apply_filters('electro_deal_save_label_show_savings', true) && $product->is_on_sale()) {
        ?>
        <div class="savings">
            <span class="savings-text"><?php echo sprintf('%s %s', '便宜', Electro_WC_Helper::get_savings_on_sale($product, $args['savings_in'])); ?></span>
        </div>
<?php
    }
}
