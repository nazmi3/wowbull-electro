<?php

/**
 * Plugin Name: HCP Eunex Customer Registration
 * Plugin URI: https://www.wowbull.com
 * Description: Override customer registration form
 * Version: 1.0
 * Author: Nazmi
 * Author URI: https://www.wowbull.com
 */

//// REMOVE UNNECESSARY FIELDS ////

add_filter('woocommerce_default_address_fields', function ($fields) {
    unset($fields['state']);
    unset($fields['city']);
    unset($fields['postcode']);
    unset($fields['address_1']);
    unset($fields['address_2']);
    unset($fields['company']);
    unset($fields['first_name']);
    unset($fields['last_name']);
    return $fields;
});

add_filter('woocommerce_billing_fields', function ($fields = array()) {
    unset($fields['billing_company']);
    unset($fields['billing_address_1']);
    unset($fields['billing_address_2']);
    unset($fields['billing_state']);
    unset($fields['billing_city']);
    unset($fields['billing_email']);
    unset($fields['billing_postcode']);
    return $fields;
});

add_filter('woocommerce_save_account_details_required_fields', function ($account_fields) {
    unset($account_fields['account_first_name']);
    unset($account_fields['account_last_name']);
    unset($account_fields['account_email']);
    return $account_fields;
});

//// REFORMAT ADDDRESS ////

add_filter('woocommerce_my_account_my_address_formatted_address', function ($args, $customer_id, $name) {
    unset($args['address_1']);
    unset($args['address_2']);
    $args['phone'] = get_user_meta($customer_id, $name . '_phone', true);
    $args['house'] = get_user_meta($customer_id, $name . '_house', true);
    $args['division'] = get_user_meta($customer_id, $name . '_division', true);
    $args['section'] = get_user_meta($customer_id, $name . '_section', true);
    return $args;
}, 10, 3);

add_filter('woocommerce_localisation_address_formats', function ($formats) {
    foreach ($formats as $key => &$format) {
        $format = "{first_name}\n{house}, {section}\n{postcode}, {division}\n{city}, {state}\n{country}\n{phone}";
    }
    return $formats;
});

add_filter('woocommerce_formatted_address_replacements', function ($replacements, $args) {
    $replacements['{phone}'] = @$args['phone'];
    $replacements['{house}'] = @$args['house'];
    $replacements['{division}'] = @$args['division'];
    $replacements['{section}'] = @$args['section'];
    return $replacements;
}, 10, 2);

//// ADD CUSTOM FIELDS ////

function add_custom_register_form_field()
{
    $billing_address = array(
        array('id' => 'reg_billing_state', 'name' => 'billing_state', 'label' => '州/省'),
        array('id' => 'reg_billing_city', 'name' => 'billing_city', 'label' => '城市'),
        array('id' => 'reg_billing_division', 'name' => 'billing_division', 'label' => '区/县'),
        array('id' => 'reg_billing_postcode', 'name' => 'billing_postcode', 'label' => '邮编'),
        array('id' => 'reg_billing_section', 'name' => 'billing_section', 'label' => '街道/镇'),
        array('id' => 'reg_billing_house', 'name' => 'billing_house', 'label' => '详细地址'),
        array('id' => 'reg_billing_phone', 'name' => 'billing_phone', 'label' => '手机号码'),
    );
    woocommerce_form_field(
        "billing_country",
        array(
            'type'        => 'country',
            'required'    => true,
            'label'       => "国家",
            'id'          => "reg_billing_country",
            'input_class' => array("input-text")
        ),
        (get_user_meta(get_current_user_id(), "billing_country", true) ?: (isset($_POST["billing_country"]) ? $_POST["billing_country"] : ''))
    );
    foreach ($billing_address as $address) {
        woocommerce_form_field(
            $address['name'],
            array(
                'type'        => 'text',
                'required'    => true,
                'label'       => $address['label'],
                'id'          => $address['id']
            ),
            (get_user_meta(get_current_user_id(), $address['name'], true) ?: (isset($_POST[$address['name']]) ? $_POST[$address['name']] : ''))
        );
    }
}
add_action('woocommerce_register_form', 'add_custom_register_form_field');

function add_custom_edit_form_field()
{
    $billing_address = array(
        array('id' => 'reg_billing_state', 'name' => 'billing_state', 'label' => '州/省'),
        array('id' => 'reg_billing_city', 'name' => 'billing_city', 'label' => '城市'),
        array('id' => 'reg_billing_division', 'name' => 'billing_division', 'label' => '区/县'),
        array('id' => 'reg_billing_postcode', 'name' => 'billing_postcode', 'label' => '邮编'),
        array('id' => 'reg_billing_section', 'name' => 'billing_section', 'label' => '街道/镇'),
        array('id' => 'reg_billing_house', 'name' => 'billing_house', 'label' => '详细地址')
    );
    foreach ($billing_address as $address) {
        woocommerce_form_field(
            $address['name'],
            array(
                'type'        => 'text',
                'required'    => true,
                'label'       => $address['label'],
                'id'          => $address['id']
            ),
            (get_user_meta(get_current_user_id(), $address['name'], true) ?: (isset($_POST[$address['name']]) ? $_POST[$address['name']] : ''))
        );
    }
}
add_action('woocommerce_after_edit_address_form_billing', 'add_custom_edit_form_field');
add_action('woocommerce_after_checkout_billing_form', 'add_custom_edit_form_field');

//// SAVE CUSTOM FIELDS //// 

function fanly_sanitize_user($username, $raw_username, $strict)
{
    $username = wp_strip_all_tags($raw_username);
    $username = remove_accents($username);
    $username = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '', $username);
    $username = preg_replace('/&.+?;/', '', $username);
    if ($strict) {
        $username = preg_replace('|[^a-z\p{Han}0-9 _.\-@]|iu', '', $username);
    }
    $username = trim($username);
    $username = preg_replace('|\s+|', ' ', $username);
    return $username;
}
add_filter('sanitize_user', 'fanly_sanitize_user', 10, 3);

function wooc_validate_extra_register_fields($username, $email, $validation_errors)
{
    if (isset($_POST['billing_country']) && empty($_POST['billing_country'])) {
        $validation_errors->add('billing_country_error', __('<strong>Error</strong>: Country is required!', 'woocommerce'));
    }
    if (isset($_POST['billing_phone']) && empty($_POST['billing_phone'])) {
        $validation_errors->add('billing_phone_error', __('<strong>Error</strong>: Phone is required!', 'woocommerce'));
    }
    return $validation_errors;
}
add_action('woocommerce_register_post', 'wooc_validate_extra_register_fields', 10, 3);
add_action('woocommerce_after_save_address_validation', 'wooc_validate_extra_register_fields', 10, 3);

function wooc_save_extra_register_fields($customer_id)
{
    if (isset($_POST['billing_country'])) {
        update_user_meta($customer_id, 'billing_country', sanitize_text_field($_POST['billing_country']));
    }
    if (isset($_POST['billing_state'])) {
        update_user_meta($customer_id, 'billing_state', sanitize_text_field($_POST['billing_state']));
    }
    if (isset($_POST['billing_city'])) {
        update_user_meta($customer_id, 'billing_city', sanitize_text_field($_POST['billing_city']));
    }
    if (isset($_POST['billing_division'])) {
        update_user_meta($customer_id, 'billing_division', sanitize_text_field($_POST['billing_division']));
    }
    if (isset($_POST['billing_section'])) {
        update_user_meta($customer_id, 'billing_section', sanitize_text_field($_POST['billing_section']));
    }
    if (isset($_POST['billing_postcode'])) {
        update_user_meta($customer_id, 'billing_postcode', sanitize_text_field($_POST['billing_postcode']));
    }
    if (isset($_POST['billing_house'])) {
        update_user_meta($customer_id, 'billing_house', sanitize_text_field($_POST['billing_house']));
    }

    if (isset($_POST['email'])) {
        update_user_meta($customer_id, 'email', sanitize_text_field($_POST['email']));
    }
    if (isset($_POST['username'])) {
        update_user_meta($customer_id, 'first_name', sanitize_text_field($_POST['username']));
        update_user_meta($customer_id, 'billing_first_name', sanitize_text_field($_POST['username']));
        update_user_meta($customer_id, 'last_name', sanitize_text_field($_POST['username']));
        update_user_meta($customer_id, 'billing_last_name', sanitize_text_field($_POST['username']));
    }
    if (isset($_POST['billing_phone'])) {
        update_user_meta($customer_id, 'billing_phone', sanitize_text_field($_POST['billing_phone']));
        update_user_meta($customer_id, 'phone', sanitize_text_field($_POST['billing_phone']));
    }
}
add_action('woocommerce_created_customer', 'wooc_save_extra_register_fields', 100);
add_action('woocommerce_customer_save_address', 'wooc_save_extra_register_fields', 100);


// remove_action('woocommerce_register_form_start', 'electro_before_register_text');
// remove_action('woocommerce_register_form', 'wc_registration_privacy_policy_text');
// remove_action('woocommerce_register_form_end', 'electro_register_benefits'); 

//// CUSTOM JS BY PAGE //// 

add_action('woocommerce_register_form_start', function () { ?>
    <script>
        jQuery(function($) {
            $("#reg_email").change(function() {
                var ajaxurl = "<?php echo site_url() ?>" + "/wp-admin/admin-ajax.php";
                var data = {
                    'action': 'check_user_email',
                    'user_email': $("#reg_email").val()
                };
                $.post(ajaxurl, data, function(response) {
                    console.log(response)
                });
            });
        });
    </script>
<?php
});

add_action('woocommerce_edit_account_form_start', function () { ?>
    <script>
        jQuery(document).ready(function($) {
            if (window.location.pathname == "/my-account/edit-account/") {
                $('input[name=account_email]').attr("disabled", "disabled");
                $('input[name=account_first_name]').attr("disabled", "disabled");
                $('input[name=account_last_name]').attr("disabled", "disabled");
                $("#account_email,#account_first_name,#account_last_name").parent().hide();
            }
        });
    </script>
<?php
});


add_filter('woocommerce_account_menu_items', 'hcp_remove_address_my_account', 999);
function hcp_remove_address_my_account($items)
{
    unset($items['downloads']);
    return $items;
}
