<?php

/**
 * Plugin Name: HCP Custom Templates
 * Plugin URI: https://www.wowbull.com
 * Description: Override Templates
 * Version: 1.0
 * Author: Nazmi
 * Author URI: https://www.wowbull.com
 */
add_filter('woocommerce_get_order_item_totals', 'reordering_order_item_totals', 10, 3);
function reordering_order_item_totals($total_rows, $order, $tax_display)
{
    global $wpdb;
    $transaction = $wpdb->get_row($wpdb->prepare("SELECT * FROM " . $wpdb->base_prefix . "transactions WHERE order_id =" . $order->get_order_number() . " AND status = 3 ORDER BY id DESC"));

    unset($total_rows['payment_method']);
    $items = $order->get_items();
    $shippping = $order->get_total_shipping();
    $subtotal_regular_price = $subtotal_sale_price = $total_discounted_price = 0;
    foreach ($items as $item) {
        $product = $item->get_product();
        $subtotal_regular_price += $product->get_regular_price();
        $subtotal_sale_price += $product->get_sale_price();
    }
    $total_regular_price = $subtotal_regular_price + $shippping;
    $total_sale_price = $subtotal_sale_price + $shippping;

    $selected_currencies = get_list_currencies();
    $wc_currency = get_option('woocommerce_currency');
    $currency_symbol = get_default_currency_symbol($wc_currency);

    $total_rows['cart_subtotal']['value'] = wc_format_sale_price($subtotal_regular_price, $subtotal_sale_price);
    $total_rows['order_total']['value'] = wc_format_sale_price($total_regular_price, $total_sale_price);
    if (@$transaction->coin_symbol == 2) {
        $total_rows['cart_subtotal']['value'] = $currency_symbol . " " . $subtotal_regular_price;
        $total_rows['order_total']['value'] = $currency_symbol . " " . $total_regular_price;
    }

    if ($transaction) {
        $base_price = number_format(($transaction->base_amount), 2, '.', ',');
        $converted_price = number_format(($transaction->amount), 2, '.', ',');
        foreach ($selected_currencies as $currencies) {
            if (($currencies['name'] == "USDT" && $transaction->coin_symbol == 2) || ($currencies['name'] == "GXM" && $transaction->coin_symbol == 1)) {
                $total_rows[$currencies['name']] = array(
                    'label' => "<img style='width:15px;height:15px' class='logo " . $currencies['name'] . "' src='" . $currencies['flag'] . "'>",
                    'value' => "<div>" . $converted_price . " (" . $currency_symbol . " " . $base_price . ")</div>",
                );
                break;
            }
        }
    } else {
        foreach ($selected_currencies as $currencies) {
            if ($currencies['name'] == "CNY") {
                continue;
            }
            if ($currencies['name'] == "GXM") {
                $base_price = number_format(($total_sale_price), 2, '.', ',');
                $converted_price = number_format(($total_sale_price  * $currencies['rate']), 2, '.', ',');
            }
            if ($currencies['name'] == "USDT") {
                $base_price = number_format(($total_regular_price), 2, '.', ',');
                $converted_price = number_format(($total_regular_price  * $currencies['rate']), 2, '.', ',');
            }
            $total_rows[$currencies['name']] = array(
                'label' => "<img style='width:15px;height:15px' class='logo " . $currencies['name'] . "' src='" . $currencies['flag'] . "'>",
                'value' => "<div>" . $converted_price . " (" . $currency_symbol . " " . $base_price . ")</div>",
            );
        }
    }
    return $total_rows;
}


function hcp_footer()
{
    $url =  wp_get_upload_dir()['baseurl'] . '/2020/11/';
?>
    <div class="row wrapper_footer_static homepage">
        <div class="footer-header">
            <div class="col-xs-12 col-sm-12 col-md-12 text-center home-footer-header">特色野牛平台</div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">品类齐全，轻松购物，为您呈现不一样的生活平台</div>
            <div class="col-md-8 col-md-offset-2">
                <div class="row text-center">
                    <div class="col-md-4 col-xs-12"> <img class="footer-img" src="<?php echo $url; ?>file_3.png" width="50" height="50">
                        <div class="">正品保障</div>
                        <div class="">正品行货，放心选购</div>
                    </div>
                    <div class="col-md-4 col-xs-12"> <img class="footer-img" src="<?php echo $url; ?>file_2.png" width="50">
                        <div class="">售后无优</div>
                        <div class="">客服全年无休, 用户体验至上</div>
                    </div>
                    <div class="col-md-4 col-xs-12"> <img class="footer-img" src="<?php echo wp_get_upload_dir()['baseurl'] ?>/2020/12/search.png" width="50">
                        <div class="">产地直采</div>
                        <div class="">100％产地直采，放心低价</div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center home-footer-header">合作伙伴</div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center merchant-footer">
                <div class="merchant-img"><img src="<?php echo $url; ?>5-5.jpg"></div>
                <div class="merchant-img"><img src="<?php echo $url; ?>7-1.jpg"></div>
                <div class="merchant-img"><img src="<?php echo $url; ?>13-1.jpg"></div>
                <div class="merchant-img"><img src="<?php echo $url; ?>14-1.jpg"></div>
                <div class="merchant-img"><img src="<?php echo $url; ?>8.png"></div>
                <div class="merchant-img"><img src="<?php echo $url; ?>9.png"></div>
                <div class="merchant-img"><img src="<?php echo $url; ?>10-1.jpg"></div>
                <div class="merchant-img"><img src="<?php echo $url; ?>12-1.jpg"></div>
                <div class="merchant-img"><img src="<?php echo $url; ?>11.png"></div>
                <div class="merchant-img"><img src="<?php echo $url; ?>3.png"></div>
                <div class="merchant-img"><img src="<?php echo $url; ?>1_1.png"></div>
                <div class="merchant-img"><img src="<?php echo $url; ?>2.png"></div>
                <div class="merchant-img"><img src="<?php echo $url; ?>4.png"></div>
            </div>
        </div>
    </div>
    <footer class="page-footer">
        <div class="footer-container">
            <div class="footer-static-container">
                <div class="container">
                    <div class="wrapper_footer_static">
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="section-menu">
                                    <div class="page-footer footer-bottom">
                                        <div class="row wrapper_footer_static">
                                            <div class="col-xs-12 col-sm-12 col-md-12 footer-header text-center">野牛商城</div>
                                        </div>
                                        <div class="row wrapper_footer_static text-center">
                                            <div class="col-xs-12 col-sm-12 col-md-12">https://wowbull.com/</div>
                                        </div>
                                        <div class="wrapper_copyright copyright_web hidden-sm text-center">Copyright © 2020 野牛商城. All Rights Reserved.</div>
                                        <!-- <div class="wrapper_copyright copyright_mobile_web hidden-md text-center">Copyright © 2020 野牛商城.<br> All Rights Reserved.</div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?php }
add_action('wp_footer', 'hcp_footer');
