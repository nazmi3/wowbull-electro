<?php

/**
 * Plugin Name: HCP Custom Currency Rate Cron Update
 * Plugin URI: https://www.wowbull.com
 * Description: Custom currency rate update
 * Version: 1.0
 * Author: Nazmi
 * Author URI: https://www.wowbull.com
 * Class customCurrency
 */

add_action('action_scheduler_run_queue', 'get_cny_usdt_rate');

function get_cny_usdt_rate()
{
    $rates = array();
    $params = get_option('eunex_url') . "callapiotc/EunexTrade/GetEunexCountryInfo";
    $args = array('headers' => array('Authorization' => "E961EB96-C507-44E5-92CB-59DD55CB5CA7"));
    $response = wp_remote_get($params, $args);
    try {
        $coins = json_decode($response['body'], TRUE);
        foreach ($coins as $coin) {
            if ($coin['currencycode'] == "CNY") {
                // $rates['usdt2cny'] = $coin['sellrate_usdt'];
                // $rates['usdt2usd'] = $coin['sellrate'] / $coin['sellrate_usdt'];
                // $rates['cny2usdt'] = 1 / $coin['sellrate_usdt'];
                $rates['usdt2cny'] = $coin['sellrate'];
                $rates['usdt2usd'] = 1;

                $rates['usd2cny'] = $coin['sellrate'];
                $rates['usd2usdt'] = 1;

                $rates['cny2usd'] = 1 / $coin['sellrate'];
                $rates['cny2usdt'] = 1 / $coin['sellrate'];
            }
        }
    } catch (Exception $ex) {
        print_r($ex);
    }
    get_gxm_rate($rates);
}

function get_gxm_rate($rates)
{
    $APIid = "1ySxuFiu33MwCF3d"; // PRODUCTION
    // $APIid = "eAq5inMsvLtfsNV5"; // STAGING
    $timenow = round(microtime(true) * 1000);
    $params = get_option('eunex_url') . "v2/market/GXM_USDT?api_id=" . $APIid . "&timestamp=" . $timenow;
    $response = wp_remote_get($params);
    try {
        $gxm = json_decode($response['body'], TRUE);
        if (@$gxm['data'][0]['price'] > 0) {
            $rates['gxm2usdt'] = $gxm['data'][0]['price'];
            $rates['usdt2gxm'] = 1 / $gxm['data'][0]['price'];
            update_rates($rates);
        }
    } catch (Exception $ex) {
        print_r($ex);
    }
}

function update_rates($rates)
{
    $currencies = get_all_currencies();
    if (get_option('usd2cny_fixed') == "no") {
        $currencies["USDT"]['rate'] = $rates['cny2usdt'];
    } else {
        $usd2cny = @get_option('fixed_usd2cny_rate') ?: $rates['usd2cny'];
        $currencies["USDT"]['rate'] = 1 / $usd2cny * $rates['usd2usdt'];
    }
    $currencies["GXM"]['rate'] = $rates['cny2usdt'] * $rates['usdt2gxm'];
    // print_r($rates);die;
    update_option('eunex_currency_rates', $rates);
    update_option('woocs', $currencies);
    die;
}

function stop_rate_update()
{
    $currencies = get_all_currencies();
    update_option('woocs', $currencies);
    exit;
}
