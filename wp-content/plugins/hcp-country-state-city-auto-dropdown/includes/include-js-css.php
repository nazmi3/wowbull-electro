<?php
/* Include all js and css files */
function hcp_csca_embedCssJs() {
	wp_enqueue_script( 'hcp_csca-district-auto-script', HCP_CSCA_URL . 'assets/js/script.min.js', array( 'jquery' ) );
	wp_localize_script( 'hcp_csca-district-auto-script', 'hcp_csca_auto_ajax', array( 'ajax_url' => admin_url('admin-ajax.php'),'nonce'=>wp_create_nonce('hcp_csca_ajax_nonce')) );
	}
add_action( 'wp_enqueue_scripts', 'hcp_csca_embedCssJs' );