<?php 
/*
check contact form 7 plugin is active.
*/
if(class_exists('WPCF7')){
	require HCP_CSCA_PATH. 'includes/district-dropdown.php';
	require HCP_CSCA_PATH. 'includes/division-dropdown.php';
	require HCP_CSCA_PATH. 'includes/section-dropdown.php';
	require HCP_CSCA_PATH. 'includes/include-js-css.php';
    require HCP_CSCA_PATH. 'includes/ajax-actions.php';
}
?>