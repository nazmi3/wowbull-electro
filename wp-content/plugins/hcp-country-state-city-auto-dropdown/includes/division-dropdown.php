<?php
/**
** [division_auto] and [division_auto*]
**/

/* form_tag handler */

add_action( 'wpcf7_init', 'hcp_csca_add_form_tag_divisionauto' );

function hcp_csca_add_form_tag_divisionauto() {
	wpcf7_add_form_tag(
		array( 'division_auto', 'division_auto*'),
		'hcp_csca_division_auto_form_tag_handler', array( 'name-attr' => true ) );
}

function hcp_csca_division_auto_form_tag_handler( $tag ) {
	if ( empty( $tag->name ) ) {
		return '';
	}
    // var_dump($tag);
	$options=$tag->options;
	$validation_error = wpcf7_get_validation_error( $tag->name );
	$class = wpcf7_form_controls_class( $tag->type, 'wpcf7-select division_auto' );
	$atts = array();
	$atts['class'] = $tag->get_class_option( $class );
	$atts['id'] = $tag->get_id_option();
	if ( $tag->is_required() ) {
		$atts['aria-required'] = 'true';
	}
	$atts['aria-invalid'] = $validation_error ? 'true' : 'false';
	
	$atts['name'] = $tag->name;
	$atts = wpcf7_format_atts( $atts );
$cnt_tag = wpcf7_scan_form_tags(array('type' => array('district_auto*','district_auto')));
if($cnt_tag)
{	
$html='<span class="wpcf7-form-control-wrap division_auto '.$tag->name.'">';
$html.='<select '.$atts.' >';
$html.='<option value="0">Select Division</option>';
$html.='</select></span>';
}
else
{
$html='Error: District Field Must be available.';	
}
return $html;
}


/* Validation filter */


add_filter( 'wpcf7_validate_division_auto', 'hcp_csca_division_auto_validation_filter', 10, 2 );
add_filter( 'wpcf7_validate_division_auto*', 'hcp_csca_division_auto_validation_filter', 10, 2 );

function hcp_csca_division_auto_validation_filter( $result, $tag ) {
	$type = $tag->type;
	$name = $tag->name;
    $value = sanitize_text_field($_POST[$name]);
	if ( $tag->is_required() && '0' == $value ) {
		$result->invalidate( $tag, 'Please Select Division.' );
	}
	

	return $result;
}



/* Tag generator */

add_action( 'wpcf7_admin_init', 'hcp_csca_add_tag_generator_Division_auto', 20 );

function hcp_csca_add_tag_generator_Division_auto() {
	$tag_generator = WPCF7_TagGenerator::get_instance();
	$tag_generator->add( 'division_auto', __( 'division drop-down', 'hcp_csca' ),
		'hcp_csca_tag_generator_division_auto' );
}

function hcp_csca_tag_generator_division_auto( $contact_form, $args = '' ) {
	$args = wp_parse_args( $args, array() );
	$type = 'division_auto';

	$description = __( "Generate a form-tag for a district dorp list with flags icon text input field.", 'hcp_csca' );
	$desc_link = '';
?>
<div class="control-box">
<fieldset>
<legend><?php echo sprintf( esc_html( $description ), $desc_link ); ?></legend>

<table class="form-table">
<tbody>
	<tr>
	<th scope="row"><?php echo esc_html( __( 'Field type', 'hcp_csca' ) ); ?></th>
	<td>
		<fieldset>
		<legend class="screen-reader-text"><?php echo esc_html( __( 'Field type', 'hcp_csca' ) ); ?></legend>
		<label><input type="checkbox" name="required" /> <?php echo esc_html( __( 'Required field', 'hcp_csca' ) ); ?></label>
		</fieldset>
	</td>
	</tr>

	<tr>
	<th scope="row"><label for="<?php echo esc_attr( $args['content'] . '-name' ); ?>"><?php echo esc_html( __( 'Name', 'hcp_csca' ) ); ?></label></th>
	<td><input type="text" name="name" class="tg-name oneline" id="<?php echo esc_attr( $args['content'] . '-name' ); ?>" /></td>
	</tr>

	
	<tr>
	<th scope="row"><label for="<?php echo esc_attr( $args['content'] . '-id' ); ?>"><?php echo esc_html( __( 'Id attribute', 'hcp_csca' ) ); ?></label></th>
	<td><input type="text" name="id" class="idvalue oneline option" id="<?php echo esc_attr( $args['content'] . '-id' ); ?>" /></td>
	</tr>

	<tr>
	<th scope="row"><label for="<?php echo esc_attr( $args['content'] . '-class' ); ?>"><?php echo esc_html( __( 'Class attribute', 'hcp_csca' ) ); ?></label></th>
	<td><input type="text" name="class" class="classvalue oneline option" id="<?php echo esc_attr( $args['content'] . '-class' ); ?>" /></td>
	</tr>

</tbody>
</table>
</fieldset>
</div>

<div class="insert-box">
	<input type="text" name="<?php echo $type; ?>" class="tag code" onfocus="this.select()" />

	<div class="submitbox">
	<input type="button" class="button button-primary insert-tag" value="<?php echo esc_attr( __( 'Insert Tag', 'hcp_csca' ) ); ?>" />
	</div>

	<br class="clear" />

	<p class="description mail-tag"><label for="<?php echo esc_attr( $args['content'] . '-mailtag' ); ?>"><?php echo sprintf( esc_html( __( "To use the value input through this field in a mail field, you need to insert the corresponding mail-tag (%s) into the field on the Mail tab.", 'hcp_csca' ) ), '<strong><span class="mail-tag"></span></strong>' ); ?><input type="text" class="mail-tag code hidden" readonly="readonly" id="<?php echo esc_attr( $args['content'] . '-mailtag' ); ?>" /></label></p>
</div>
<?php
}