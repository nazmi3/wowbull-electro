<?php
add_action('wp_ajax_hcp_csca_get_country', 'hcp_csca_get_country');
add_action("wp_ajax_nopriv_hcp_csca_get_country", "hcp_csca_get_country");
function hcp_csca_get_country()
{
    check_ajax_referer('hcp_csca_ajax_nonce', 'nonce_ajax');
    global $wpdb;
    $raw_countries =  WC()->countries->get_allowed_countries();

    if ($raw_countries) {
        $lists = "IN(";
        $i = 1;
        foreach ($raw_countries as $ckey => $cval) {
            $lists .= ($i == 1 ?  "" : ",") . "'" . $ckey . "'";
            $i++;
        }
        $lists .= ")";
        $country = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . $wpdb->base_prefix . "countries WHERE code $lists order by name asc"));
    }
    echo json_encode($country);
    wp_die();
}

add_action('wp_ajax_hcp_csca_get_states', 'hcp_csca_get_states');
add_action("wp_ajax_nopriv_hcp_csca_get_states", "hcp_csca_get_states");
function hcp_csca_get_states()
{
    check_ajax_referer('hcp_csca_ajax_nonce', 'nonce_ajax');
    global $wpdb;
    if (isset($_POST["cnt"])) {
        $cid = sanitize_text_field($_POST["cnt"]);
    }
    $states = $wpdb->get_results($wpdb->prepare("SELECT st.* FROM " . $wpdb->base_prefix . "state st 
    left join " . $wpdb->base_prefix . "countries cnt ON st.country_id = cnt.id 
    where cnt.code='%1s' order by st.name asc", $cid));
    echo json_encode($states);
    wp_die();
}

add_action('wp_ajax_hcp_csca_get_cities', 'hcp_csca_get_cities');
add_action("wp_ajax_nopriv_hcp_csca_get_cities", "hcp_csca_get_cities");
function hcp_csca_get_cities()
{
    check_ajax_referer('hcp_csca_ajax_nonce', 'nonce_ajax');
    global $wpdb;
    if (isset($_POST["sid"])) {
        $sid = sanitize_text_field($_POST["sid"]);
    }
    $cities = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . $wpdb->base_prefix . "city where state_id=%1s order by name asc", $sid));
    echo json_encode($cities);
    wp_die();
}

add_action('wp_ajax_hcp_csca_get_division', 'hcp_csca_get_division');
add_action("wp_ajax_nopriv_hcp_csca_get_division", "hcp_csca_get_division");
function hcp_csca_get_division()
{
    check_ajax_referer('hcp_csca_ajax_nonce', 'nonce_ajax');
    global $wpdb;
    if (isset($_POST["cnt"])) {
        $cid = sanitize_text_field($_POST["cnt"]);
    }
    $division = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . $wpdb->base_prefix . "division where city_id=%1s order by name asc", $cid));
    echo json_encode($division);
    wp_die();
}

add_action('wp_ajax_hcp_csca_get_section', 'hcp_csca_get_section');
add_action("wp_ajax_nopriv_hcp_csca_get_section", "hcp_csca_get_section");
function hcp_csca_get_section()
{
    check_ajax_referer('hcp_csca_ajax_nonce', 'nonce_ajax');
    global $wpdb;
    if (isset($_POST["sid"])) {
        $sid = sanitize_text_field($_POST["sid"]);
    }
    $section = $wpdb->get_results($wpdb->prepare("SELECT * FROM " . $wpdb->base_prefix . "section where division_id=%1s order by name asc", $sid));
    echo json_encode($section);
    wp_die();
}
