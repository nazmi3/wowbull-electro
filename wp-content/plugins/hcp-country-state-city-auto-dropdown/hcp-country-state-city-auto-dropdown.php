<?php
/*
Plugin Name: Custom District Division Section Dropdown CF7
Description: Add district, division and section auto drop down for CONTACT FORM 7. Division will auto populate in SELECT field according to selected district and section will auto populate according to selected division. 
Version: 2.3
Author: Nazmi
Author URI: http://wowbull.com 
*/
// Block direct access to the main plugin file.
defined('ABSPATH') or die('No tircks please!');

class HCP_CSCA_Plugin
{

	public function __construct()
	{
		add_action('plugins_loaded', array($this, 'hcp_load_plugin_textdomain'));
		if (class_exists('WPCF7')) {
			$this->hcp_plugin_constants();
			require_once HCP_CSCA_PATH . 'includes/autoload.php';

			add_action('admin_enqueue_scripts', array($this, 'add_scripts_and_styles'));
		} else {
			add_action('admin_notices', array($this, 'hcp_admin_error_notice'));
		}
		if (!get_option('hcp_auto_plugin_version')) {
			global $wpdb;
			$charset_collate = $wpdb->get_charset_collate();
			$table_country = $wpdb->prefix . 'countries';
			$table_state = $wpdb->prefix . 'state';
			$table_city = $wpdb->prefix . 'city';
			$table_district = $wpdb->prefix . 'district';
			$table_division = $wpdb->prefix . 'division';
			$table_section = $wpdb->prefix . 'section';

			$country_create = "CREATE TABLE IF NOT EXISTS $table_country (
								`id` int(11) NOT NULL AUTO_INCREMENT,
								`name` varchar(100) NOT NULL,
								`code` varchar(100) NOT NULL,
								`label` varchar(100) NOT NULL,
								PRIMARY KEY (id)
								) ENGINE=InnoDB DEFAULT CHARSET=utf8";
			$state_create = "CREATE TABLE IF NOT EXISTS $table_state (
							`id` int(11) NOT NULL AUTO_INCREMENT,
							`name` varchar(255) NOT NULL,
							`country_id` mediumint(8) unsigned NOT NULL,
							PRIMARY KEY (id)
							) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;";
			$city_create = "CREATE TABLE IF NOT EXISTS $table_city (
							`id` int(11) NOT NULL AUTO_INCREMENT,
							`name` varchar(255) NOT NULL,
							`state_id` mediumint(8) unsigned NOT NULL,
							PRIMARY KEY (id)
							) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;";
			$district_create = "CREATE TABLE IF NOT EXISTS $table_district (
								`id` int(11) NOT NULL AUTO_INCREMENT,
								`name` varchar(100) NOT NULL,
								`city_id` mediumint(8) unsigned NOT NULL,
								PRIMARY KEY (id)
								) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
			$division_create = "CREATE TABLE IF NOT EXISTS $table_division (
								`id` int(11) NOT NULL AUTO_INCREMENT,
								`name` varchar(255) NOT NULL,
								`city_id` mediumint(8) unsigned NOT NULL,
								PRIMARY KEY (id)
								) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;";
			$section_create = "CREATE TABLE IF NOT EXISTS $table_section (
								`id` int(11) NOT NULL AUTO_INCREMENT,
								`name` varchar(255) NOT NULL,
								`division_id` mediumint(8) unsigned NOT NULL,
								PRIMARY KEY (id)
								) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;";

			$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}countries");
			$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}state");
			$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}city");
			$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}district");
			$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}division");
			$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}section");

			include_once('includes/countries-sql.php');
			include_once('includes/states-sql.php');
			include_once('includes/cities-sql.php');
			include_once('includes/district-sql.php');
			include_once('includes/division-sql.php');
			include_once('includes/section-sql.php');

			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($country_create);
			dbDelta($state_create);
			dbDelta($city_create);
			dbDelta($district_create);
			dbDelta($division_create);
			dbDelta($section_create);

			dbDelta($country_insert);
			dbDelta($state_insert);
			dbDelta($city_insert);
			dbDelta($district_insert);
			dbDelta($division_insert);
			dbDelta($section_insert);
			update_option('hcp_auto_plugin_version', '2.3');
		}
	}


	public function hcp_load_plugin_textdomain()
	{
		// load_plugin_textdomain( 'hcp_csca', false, basename( dirname( __FILE__ ) ) . '/languages/' );
	}

	/*
		register admin notice if contact form 7 is not active.
	*/

	public function hcp_admin_error_notice()
	{
		$message = sprintf(esc_html__('The %1$sDistrict Division Section Dropdown CF7%2$s plugin requires %1$sContact form 7%2$s plugin active to run properly. Please install %1$scontact form 7%2$s and activate', 'hcp_csca'), '<strong>', '</strong>');

		printf('<div class="notice notice-error"><p>%1$s</p></div>', wp_kses_post($message));
	}

	/*
		set plugin constants
	*/
	public function hcp_plugin_constants()
	{

		if (!defined('HCP_CSCA_PATH')) {
			define('HCP_CSCA_PATH', plugin_dir_path(__FILE__));
		}
		if (!defined('HCP_CSCA_URL')) {
			define('HCP_CSCA_URL', plugin_dir_url(__FILE__));
		}
	}
	/*
	Enqueue Scripts For ADMIN
	*/
	public function add_scripts_and_styles($hook)
	{
		// Only load on ?page=wpcf7&post=??
		if (strpos($hook, 'wpcf7') !== false && isset($_GET['post'])) {
			$localize = array('csca_metabox' => hcp_auto_plugin_add_post_metabox());
			wp_enqueue_script('hcp_csca-district-auto-script-meta', HCP_CSCA_URL . 'assets/js/script-meta.min.js', array('jquery'));
			wp_localize_script('hcp_csca-district-auto-script-meta', 'hcp_csca_auto_ajax_meta', $localize);
		}
	}
}

// Instantiate the plugin class.
$hcp_csca_plugin = new HCP_CSCA_Plugin();
register_activation_hook(__FILE__, 'hcp_create_db');
function hcp_create_db()
{
	global $wpdb;
	$charset_collate = $wpdb->get_charset_collate();
	$table_country = $wpdb->prefix . 'countries';
	$table_state = $wpdb->prefix . 'state';
	$table_city = $wpdb->prefix . 'city';
	$table_district = $wpdb->prefix . 'district';
	$table_division = $wpdb->prefix . 'division';
	$table_section = $wpdb->prefix . 'section';


	$country_create = "CREATE TABLE IF NOT EXISTS $table_country (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`name` varchar(100) NOT NULL,
					`code` varchar(100) NOT NULL,
					`label` varchar(100) NOT NULL,
					PRIMARY KEY (id)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8";
	$state_create = "CREATE TABLE IF NOT EXISTS $table_state (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`name` varchar(255) NOT NULL,
					`country_id` mediumint(8) unsigned NOT NULL,
					PRIMARY KEY (id)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;";
	$city_create = "CREATE TABLE IF NOT EXISTS $table_city (
					`id` int(11) NOT NULL AUTO_INCREMENT,
					`name` varchar(255) NOT NULL,
					`state_id` mediumint(8) unsigned NOT NULL,
					PRIMARY KEY (id)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;";
	$district_create = "CREATE TABLE IF NOT EXISTS $table_district (
						`id` int(11) NOT NULL AUTO_INCREMENT,
						`name` varchar(100) NOT NULL,
						`city_id` mediumint(8) unsigned NOT NULL,
						PRIMARY KEY (id)
						) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
	$division_create = "CREATE TABLE IF NOT EXISTS $table_division (
						`id` int(11) NOT NULL AUTO_INCREMENT,
						`name` varchar(255) NOT NULL,
						`city_id` mediumint(8) unsigned NOT NULL,
						PRIMARY KEY (id)
						) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;";
	$section_create = "CREATE TABLE IF NOT EXISTS $table_section (
						`id` int(11) NOT NULL AUTO_INCREMENT,
						`name` varchar(255) NOT NULL,
						`division_id` mediumint(8) unsigned NOT NULL,
						PRIMARY KEY (id)
						) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;";

	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}countries");
	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}state");
	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}city");
	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}district");
	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}division");
	$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}section");

	include_once('includes/countries-sql.php');
	include_once('includes/states-sql.php');
	include_once('includes/cities-sql.php');
	include_once('includes/district-sql.php');
	include_once('includes/division-sql.php');
	include_once('includes/section-sql.php');
	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

	dbDelta($country_create);
	dbDelta($state_create);
	dbDelta($city_create);
	dbDelta($district_create);
	dbDelta($division_create);
	dbDelta($section_create);

	// if (!get_option('hcp_auto_plugin')) {
	// 	update_option('hcp_auto_plugin', 'installed');
	// }
	// if (get_option('hcp_auto_plugin') == 'installed') {
	dbDelta($country_insert);
	dbDelta($state_insert);
	dbDelta($city_insert);
	dbDelta($district_insert);
	dbDelta($division_insert);
	dbDelta($section_insert);
	// }
	update_option('hcp_auto_plugin', 'activated');
	update_option('hcp_auto_plugin_version', '2.3');
	$notices[] = "<b>Trusty Plugins</b> : <b style='color:#bb2c2c;'>CHEERS!!</b> Test and Rate ★★★★★ Our Plugin <b>District Division Section Dropdown CF7</b> on <a href='https://wordpress.org/support/plugin/district-division-section-auto-dropdown/reviews/?filter=5#new-post' style='color:#bb2c2c;font-weight:bold;' target='_blank'>Wordpress.org</a> or <a href='http://trustyplugins.com#extend' style='color:#bb2c2c;font-weight:bold;' target='_blank'>BUY PRO</a> to extend its features.";
	update_option('hcp_auto_plugin_admin_notices', $notices);
}

/* PROMO CODE */

add_action('admin_notices', 'hcp_auto_plugin_admin_notices');
function hcp_auto_plugin_admin_notices()
{
	if ($notices = get_option('hcp_auto_plugin_admin_notices')) {
		foreach ($notices as $notice) {
			echo "<div class='updated'><p>$notice</p></div>";
		}
		delete_option('hcp_auto_plugin_admin_notices');
	}
}


function hcp_auto_plugin_add_post_metabox()
{
	ob_start();
?>
	<div id="hcp_auto_plugin_meta_pro" class="hcp_auto_plugin_meta_pro postbox">
	</div>
<?php
	$output = ob_get_contents();
	ob_end_clean();
	return $output;
}
?>