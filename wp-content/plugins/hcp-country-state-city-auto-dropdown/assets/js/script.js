jQuery(function ($) {

    // var country_dropdown = '<span class="wpcf7-form-control-wrap country_custom"><select name="billing_country" class="wpcf7-form-control wpcf7-select country_custom wpcf7-country_custom input-text" id="country-custom" aria-invalid="false"></select></span>';
    // $("#reg_billing_country").after(country_dropdown);
    var state_dropdown = '<span class="wpcf7-form-control-wrap state_auto"><select name="state_auto" class="wpcf7-form-control wpcf7-select state_auto wpcf7-state_auto input-text" id="state-options" aria-invalid="false"><option value="0">请选择一个省/州。</option></select></span>';
    $("#reg_billing_state").after(state_dropdown);
    var city_dropdown = '<span class="wpcf7-form-control-wrap city_auto"><select name="city_auto" class="wpcf7-form-control wpcf7-select city_auto wpcf7-city_auto input-text" id="city-options" aria-invalid="false"><option value="0">请选择一个城市。</option></select></span>';
    $("#reg_billing_city").after(city_dropdown);
    var division_dropdown = '<span class="wpcf7-form-control-wrap division_auto"><select name="division_auto" class="wpcf7-form-control wpcf7-select division_auto wpcf7-division_auto input-text" id="division-options" aria-invalid="false"><option value="0">请选择一个区/县。</option></select></span>';
    $("#reg_billing_division").after(division_dropdown);
    var section_dropdown = '<span class="wpcf7-form-control-wrap section_auto"><select name="section_auto" class="wpcf7-form-control wpcf7-select section_auto wpcf7-section_auto input-text" id="section-options" aria-invalid="false"><option value="0">请选择一个街道/镇。</option></select></span>';
    $("#reg_billing_section").after(section_dropdown);

    // $("#reg_billing_country").hide();
    $("#reg_billing_state").hide();
    $("#reg_billing_city").hide();
    $("#reg_billing_division").hide();
    $("#reg_billing_section").hide();

    $('#reg_billing_phone').on('keyup', function () {
        var telephone = $('#reg_billing_phone').val();
        if (!Number.isInteger(telephone)) {
            $('#reg_billing_phone').val(telephone.replace(/\D/g, ''));
        }
    });

    // $('select[name=billing_country]').change(function () {
    //     console.log("billing_country", $('select[name=billing_country]').val())
    //     // $('body').trigger('update_checkout');
    // });

    /* --------------------- ON READY ------------------------ */

    /* --------------------- GET STATES FROM COUNTRY ID ------------------------ */
    $(document).ready(function () {
        var state = $("#reg_billing_state").val();
        if (state) {
            $("#reg_billing_state, #reg_billing_city, #reg_billing_division, #reg_billing_section").show();
            $("span.wpcf7-form-control-wrap.state_auto, span.wpcf7-form-control-wrap.city_auto, span.wpcf7-form-control-wrap.division_auto, span.wpcf7-form-control-wrap.section_auto").hide();
            // var cnt = $("select.country_custom").children("option:selected").attr('data-id');
            var cnt = $("select[name=billing_country]").children("option:selected").val();
            $("select.state_auto").html('<option value="0">请选择一个省/州。</option>');
            $("select.city_auto").html('<option value="0">请选择一个城市。</option>');
            $.ajax({
                url: hcp_csca_auto_ajax.ajax_url,
                type: 'post',
                dataType: "json",
                data: { action: "hcp_csca_get_states", nonce_ajax: hcp_csca_auto_ajax.nonce, cnt: cnt },
                success: function (response) {
                    var all_st = [];
                    for (i = 0; i < response.length; i++) {
                        var st_id = response[i]['id'];
                        var st_name = response[i]['name'];
                        all_st.push(st_name);
                        var opt = "<option data-id='" + st_id + "' value='" + st_name + "'>" + st_name + "</option>";
                        $("select.state_auto").append(opt);
                        $("#reg_billing_state, #reg_billing_city, #reg_billing_division, #reg_billing_section").hide();
                        $("span.wpcf7-form-control-wrap.state_auto, span.wpcf7-form-control-wrap.city_auto, span.wpcf7-form-control-wrap.division_auto, span.wpcf7-form-control-wrap.section_auto").show();
                    }
                    if (all_st.includes(state)) {
                        $('#state-options').val(state);
                        // $("#reg_billing_country").val($("select.country_custom").val());
                        ready_city();
                    }
                }
            });
        }

        /* --------------------- GET CITIES ------------------------ */
        function ready_city() {
            var city = $("#reg_billing_city").val();
            if (city) {
                $("#reg_billing_city, #reg_billing_division, #reg_billing_section").show();
                $("span.wpcf7-form-control-wrap.city_auto, span.wpcf7-form-control-wrap.division_auto, span.wpcf7-form-control-wrap.section_auto").hide();
                var sid = $(this).children("option:selected").attr('data-id');
                $("select.city_auto").html('<option value="0">请选择一个城市。</option>');
                $.ajax({
                    url: hcp_csca_auto_ajax.ajax_url,
                    type: 'post',
                    dataType: "json",
                    data: { action: "hcp_csca_get_cities", nonce_ajax: hcp_csca_auto_ajax.nonce, sid: sid },
                    success: function (response) {
                        var all_ct = [];
                        for (i = 0; i < response.length; i++) {
                            var ct_id = response[i]['id'];
                            var ct_name = response[i]['name'];
                            all_ct.push(ct_name);
                            var opt = "<option value='" + ct_name + "' data-id='" + ct_id + "'>" + ct_name + "</option>";
                            $("select.city_auto").append(opt);
                            $("#reg_billing_city, #reg_billing_division, #reg_billing_section").hide();
                            $("span.wpcf7-form-control-wrap.city_auto, span.wpcf7-form-control-wrap.division_auto, span.wpcf7-form-control-wrap.section_auto").show();
                        }
                        if (all_ct.includes(city)) {
                            $('#city-options').val(city);
                            $("#reg_billing_state").val($("select.state_auto").val());
                            ready_division();
                        }
                    }
                });
            }
        }

        /* --------------------- GET DIVISION FROM CITY ID ------------------------ */
        function ready_division() {
            var division = $("#reg_billing_division").val();
            if (division) {
                $("#reg_billing_division, #reg_billing_section").show();
                $("span.wpcf7-form-control-wrap.division_auto, span.wpcf7-form-control-wrap.section_auto").hide();
                var cnt = $("select.city_auto").children("option:selected").attr('data-id');
                $("select.division_auto").html('<option value="0">请选择一个区/县。</option>');
                $("select.section_auto").html('<option value="0">请选择一个街道/镇。</option>');
                $.ajax({
                    url: hcp_csca_auto_ajax.ajax_url,
                    type: 'post',
                    dataType: "json",
                    data: { action: "hcp_csca_get_division", nonce_ajax: hcp_csca_auto_ajax.nonce, cnt: cnt },
                    success: function (response) {
                        var all_st = [];
                        for (i = 0; i < response.length; i++) {
                            var st_id = response[i]['id'];
                            var st_name = response[i]['name'];
                            all_st.push(st_name);
                            var opt = "<option data-id='" + st_id + "' value='" + st_name + "'>" + st_name + "</option>";
                            $("select.division_auto").append(opt);
                            $("#reg_billing_division, #reg_billing_section").hide();
                            $("span.wpcf7-form-control-wrap.division_auto, span.wpcf7-form-control-wrap.section_auto").show();
                        }
                        if (all_st.includes(division)) {
                            $('#division-options').val(division);
                            $("#reg_billing_city").val($("select.city_auto").val());
                            ready_section();
                        }
                    }
                });
            }
        }

        /* --------------------- GET SECTION ------------------------ */
        function ready_section() {
            var section = $("#reg_billing_section").val();
            if (section) {
                $("#reg_billing_section").show();
                $("span.wpcf7-form-control-wrap.section_auto").hide();
                var sid = $(this).children("option:selected").attr('data-id');
                $("select.section_auto").html('<option value="0">请选择一个街道/镇。</option>');
                $.ajax({
                    url: hcp_csca_auto_ajax.ajax_url,
                    type: 'post',
                    dataType: "json",
                    data: { action: "hcp_csca_get_section", nonce_ajax: hcp_csca_auto_ajax.nonce, sid: sid },
                    success: function (response) {
                        var all_ct = [];
                        for (i = 0; i < response.length; i++) {
                            var ct_id = response[i]['id'];
                            var ct_name = response[i]['name'];
                            all_ct.push(ct_name);
                            var opt = "<option value='" + ct_name + "' data-id='" + ct_id + "'>" + ct_name + "</option>";
                            $("select.section_auto").append(opt);
                            $("#reg_billing_section").hide();
                            $("span.wpcf7-form-control-wrap.section_auto").show();
                        }
                        if (all_ct.includes(section)) {
                            $('#section-options').val(section);
                            $("#reg_billing_division").val($("select.division_auto").val());
                        }
                    }
                });
            }
        }
    });

    /* --------------------- ON CHANGE ------------------------ */

    /* --------------------- GET STATES FROM COUNTRY ID ------------------------ */
    $("select[name=billing_country]").change(function () {
        event.preventDefault();
        if ($("select.state_auto").length > 0) {
            var state = $("#reg_billing_state").val();
            $("#reg_billing_state, #reg_billing_city, #reg_billing_division, #reg_billing_section").show();
            $("span.wpcf7-form-control-wrap.state_auto, span.wpcf7-form-control-wrap.city_auto, span.wpcf7-form-control-wrap.division_auto, span.wpcf7-form-control-wrap.section_auto").hide();
            // var cnt = $("select.country_custom").children("option:selected").attr('data-id');
            var cnt = $("select[name=billing_country]").children("option:selected").val();
            $("select.state_auto").html('<option value="0">请选择一个省/州。</option>');
            $("select.city_auto").html('<option value="0">请选择一个城市。</option>');
            $.ajax({
                url: hcp_csca_auto_ajax.ajax_url,
                type: 'post',
                dataType: "json",
                data: { action: "hcp_csca_get_states", nonce_ajax: hcp_csca_auto_ajax.nonce, cnt: cnt },
                success: function (response) {
                    for (i = 0; i < response.length; i++) {
                        var st_id = response[i]['id'];
                        var st_name = response[i]['name'];
                        var opt = "<option data-id='" + st_id + "' value='" + st_name + "'>" + st_name + "</option>";
                        $("select.state_auto").append(opt);
                        $("#reg_billing_state, #reg_billing_city, #reg_billing_division, #reg_billing_section").hide();
                        $("span.wpcf7-form-control-wrap.state_auto, span.wpcf7-form-control-wrap.city_auto, span.wpcf7-form-control-wrap.division_auto, span.wpcf7-form-control-wrap.section_auto").show();
                    }
                    // $("#reg_billing_country").val($("select.country_custom").val());
                    $("#reg_billing_state").val("");
                    $("#reg_billing_city").val("");
                    $("#reg_billing_division").val("");
                    $("#reg_billing_section").val("");
                }
            });
        }
    });

    /* --------------------- GET CITIES ------------------------ */
    $("select.state_auto").change(function () {
        event.preventDefault();
        if ($("select.city_auto").length > 0) {
            var city = $("#reg_billing_city").val();
            $("#reg_billing_city, #reg_billing_division, #reg_billing_section").show();
            $("span.wpcf7-form-control-wrap.city_auto, span.wpcf7-form-control-wrap.division_auto, span.wpcf7-form-control-wrap.section_auto").hide();
            var sid = $(this).children("option:selected").attr('data-id');
            $("select.city_auto").html('<option value="0">请选择一个城市。</option>');
            $.ajax({
                url: hcp_csca_auto_ajax.ajax_url,
                type: 'post',
                dataType: "json",
                data: { action: "hcp_csca_get_cities", nonce_ajax: hcp_csca_auto_ajax.nonce, sid: sid },
                success: function (response) {
                    for (i = 0; i < response.length; i++) {
                        var ct_id = response[i]['id'];
                        var ct_name = response[i]['name'];
                        var opt = "<option value='" + ct_name + "' data-id='" + ct_id + "'>" + ct_name + "</option>";
                        $("select.city_auto").append(opt);
                        $("#reg_billing_city, #reg_billing_division, #reg_billing_section").hide();
                        $("span.wpcf7-form-control-wrap.city_auto, span.wpcf7-form-control-wrap.division_auto, span.wpcf7-form-control-wrap.section_auto").show();
                    }
                    $("#reg_billing_state").val($("select.state_auto").val());
                    $("#reg_billing_city").val("");
                    $("#reg_billing_division").val("");
                    $("#reg_billing_section").val("");
                }
            });
        }
    });

    /* --------------------- GET DIVISION FROM CITY ID ------------------------ */
    $("select.city_auto").change(function () {
        if ($("select.division_auto").length > 0) {
            var division = $("#reg_billing_division").val();
            $("#reg_billing_division, #reg_billing_section").show();
            $("span.wpcf7-form-control-wrap.division_auto, span.wpcf7-form-control-wrap.section_auto").hide();
            var cnt = $("select.city_auto").children("option:selected").attr('data-id');
            $("select.division_auto").html('<option value="0">请选择一个区/县。</option>');
            $("select.section_auto").html('<option value="0">请选择一个街道/镇。</option>');
            $.ajax({
                url: hcp_csca_auto_ajax.ajax_url,
                type: 'post',
                dataType: "json",
                data: { action: "hcp_csca_get_division", nonce_ajax: hcp_csca_auto_ajax.nonce, cnt: cnt },
                success: function (response) {
                    for (i = 0; i < response.length; i++) {
                        var st_id = response[i]['id'];
                        var st_name = response[i]['name'];
                        var opt = "<option data-id='" + st_id + "' value='" + st_name + "'>" + st_name + "</option>";
                        $("select.division_auto").append(opt);
                        $("#reg_billing_division, #reg_billing_section").hide();
                        $("span.wpcf7-form-control-wrap.division_auto, span.wpcf7-form-control-wrap.section_auto").show();
                    }
                    $("#reg_billing_city").val($("select.city_auto").val());
                    $("#reg_billing_division").val("");
                    $("#reg_billing_section").val("");
                }
            });
        }
    });

    /* --------------------- GET SECTION ------------------------ */
    $("select.division_auto").change(function () {
        if ($("select.section_auto").length > 0) {
            var section = $("#reg_billing_section").val();
            $("#reg_billing_section").show();
            $("span.wpcf7-form-control-wrap.section_auto").hide();
            var sid = $(this).children("option:selected").attr('data-id');
            $("select.section_auto").html('<option value="0">请选择一个街道/镇。</option>');
            $.ajax({
                url: hcp_csca_auto_ajax.ajax_url,
                type: 'post',
                dataType: "json",
                data: { action: "hcp_csca_get_section", nonce_ajax: hcp_csca_auto_ajax.nonce, sid: sid },
                success: function (response) {
                    for (i = 0; i < response.length; i++) {
                        var ct_id = response[i]['id'];
                        var ct_name = response[i]['name'];
                        var opt = "<option value='" + ct_name + "' data-id='" + ct_id + "'>" + ct_name + "</option>";
                        $("select.section_auto").append(opt);
                        $("#reg_billing_section").hide();
                        $("span.wpcf7-form-control-wrap.section_auto").show();
                    }
                    $("#reg_billing_division").val($("select.division_auto").val());
                    $("#reg_billing_section").val("");
                }
            });
        }
    });

    $("select.section_auto").change(function () {
        $("#reg_billing_section").val($("select.section_auto").val());
    });

});