<?php
if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}
$option_name = 'hcp_auto_plugin';
$version = 'hcp_auto_plugin_version';
delete_option($option_name);
delete_option($version);
global $wpdb;
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}district");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}division");
$wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}section");