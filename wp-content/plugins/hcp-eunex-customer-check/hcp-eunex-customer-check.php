<?php

/**
 * Plugin Name: HCP Eunex Check Customer Existence
 * Plugin URI: https://www.wowbull.com
 * Description: Check if customer registered on Eunex
 * Version: 1.0
 * Author: Nazmi
 * Author URI: https://www.wowbull.com
 */

add_action('wp_ajax_nopriv_check_user_email', 'check_user_email_callback');
function check_user_email_callback()
{
    global $wpdb;
    $customerEmail = urlencode($_POST['user_email']);
    if (email_exists($customerEmail)) {
        echo json_encode("此电子邮件地址的帐户已在我们的系统中注册");
    } else {
        $params = get_option('eunex_url') . "api/auth/open/member/" . $customerEmail;
        $response = wp_remote_get($params);
        try {
            $eunex = json_decode($response['body']);
            if ($eunex->exists && $eunex->activated) {
                echo true;
            } else {
                echo json_encode('注册只限给Eunex 用户. 请先注册在 <a href="' . get_option("eunex_url")  . '" target="_blank">Eunex</a>');
            }
        } catch (Exception $ex) {
            echo json_encode("错误");
        }
    }
    wp_die();
}

add_filter('woocommerce_registration_errors', 'myplugin_check_fields', 10, 3);
function myplugin_check_fields($errors, $sanitized_user_login, $user_email)
{
    global $wpdb;
    $customerEmail = urlencode($user_email);
    if (email_exists($customerEmail)) {
        $errors->add('email_error', __('此电子邮件地址的帐户已在我们的系统中注册', 'my_textdomain'));
    } else {
        $params = get_option('eunex_url') . "api/auth/open/member/" . $customerEmail;
        $response = wp_remote_get($params);
        try {
            $eunex = json_decode($response['body']);
            if ($eunex->exists && $eunex->activated) {
                return $errors;
            } else {
                $errors->add('email_error', __('注册只限给Eunex 用户. 请先注册在 <a href="' . get_option("eunex_url")  . '" target="_blank">Eunex</a>', 'my_textdomain'));
            }
        } catch (Exception $ex) {
            $errors->add('email_error', __('错误', 'my_textdomain'));
        }
    }
    return $errors;
}

add_action('user_register', 'function_new_user');
function function_new_user($user_id)
{
    add_user_meta($user_id, '_new_user', '1');
}

add_filter('wp_authenticate_user', 'myplugin_auth_login', 10, 2);
function myplugin_auth_login($user, $password)
{
    $user_new = get_user_meta($user->ID, '_new_user', true);
    if (!wp_check_password($password, $user->user_pass, $user->ID)) {
        if ($user_new) {
            return new WP_Error(
                'incorrect_password',
                ' <a href="' . wp_lostpassword_url() . '">' .
                    '您好，为了提供您更优质的购物体验，系统进行了优化升级。
                请重新设置您的用户密码，享受更安全的购物乐趣。点击“忘记密码”更新' .
                    '</a>'
            );
        }
    }
    update_user_meta($user->ID, '_new_user', '0');
    return $user;
}
