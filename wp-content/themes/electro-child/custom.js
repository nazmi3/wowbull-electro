//// PRODUCT PAGE ////
var $ = jQuery;
console.log("custom")
if ($("table").hasClass("variations")) {
    $("body.product-template-default.single.single-product p.price").hide();
}

$('#billing_phone').on('keyup', function () {
    var telephone = $('#billing_phone').val();
    if (!Number.isInteger(telephone)) {
        $('#billing_phone').val(telephone.replace(/\D/g, ''));
    }
});
