<?php

/**
 * Electro Child
 *
 * @package electro-child
 */

/**
 * Include all your custom code here
 */

add_action('wp_enqueue_scripts', 'enqueue_parent_styles');
function enqueue_parent_styles()
{
   $cache_buster = date("YmdHi", filemtime(get_stylesheet_directory() . '/style.css'));
   wp_enqueue_style('main', get_stylesheet_directory_uri() . '/style.css', array(), $cache_buster, 'all');
}

add_action('wp_enqueue_scripts', 'enqueue_custom_js');

function enqueue_custom_js()
{
   $cache_buster = date("YmdHi", filemtime(get_stylesheet_directory() . '/custom.js'));
   wp_enqueue_script('main', get_stylesheet_directory_uri() . '/custom.js', array('jquery'), $cache_buster, true);
}
